﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ChickenKitchen
{
    public class PerformAuditCommand
    {
        List<string> _auditMemory;
        public PerformAuditCommand()
        {
            _auditMemory = new List<string>();
        }
        public string InitAudit()
        {
            new PerformAuditCommand();
            return "Audit is on";
        }
        //tworzenie początkowego wpisu do audytu
        public string MakeAuditInitRecord(Warehouse warehouse, RestaurantBudget restaurantBudget, Config config)
        {
            string wastedLog = new WarehouseStorage().GetWastedTotalLog(warehouse, config);

            string initAuditRecord = "INIT" + System.Environment.NewLine + System.Environment.NewLine +
                wastedLog + System.Environment.NewLine +
                "Warehouse: " + warehouse.GetWarehouseRecord() + System.Environment.NewLine +
                "Budget: " + restaurantBudget.GetRestaurantBudget() + System.Environment.NewLine +
                System.Environment.NewLine + "START" + System.Environment.NewLine +
                System.Environment.NewLine;
            return initAuditRecord;
        }
        public void AddToAuditMemory(string auditRecord, Warehouse warehouse, RestaurantBudget restaurantBudget, Config config)
        {
            string wastedLog = new WarehouseStorage().GetWastedTotalLog(warehouse, config);

            auditRecord +=
                System.Environment.NewLine +
                wastedLog + System.Environment.NewLine +
                "Warehouse: " + warehouse.GetWarehouseRecord() + System.Environment.NewLine +
                "Budget: " + restaurantBudget.GetRestaurantBudget() + System.Environment.NewLine +
                System.Environment.NewLine; 
            _auditMemory.Add(auditRecord);
        }
        public void AddToAuditMemoryPlain(string auditRecord){_auditMemory.Add(auditRecord);}
        public List<string> GetFromMemory() { return _auditMemory; }

        public void AuditOperation_PerformBudgetCommand(string whoBuyWhat, int beforRestaurantBudget,
            string budgetOperation, int value, int afterRestaurantBudget, string isSuccess,
            Warehouse warehouse, RestaurantBudget restaurantBudget, Config config)
        {
            string auditRecord = whoBuyWhat + " -> " + beforRestaurantBudget + " " + budgetOperation + " " + value + " -> " + isSuccess + afterRestaurantBudget;
            AddToAuditMemory(auditRecord, warehouse, restaurantBudget, config);
        }

        public void AuditOperation_PerformBuy(string whoBuyWhat, string customerName, int customerFunds, string dishName, int dishPrice, string isOrderSuccess,
            int earnedMoney, int tax, int tip, Warehouse warehouse, RestaurantBudget restaurantBudget, Config config)
        {
            string auditRecord = whoBuyWhat + " -> " +
                customerName + ", " + customerFunds + ", " + dishName + ", " + dishPrice + " -> " +
                isOrderSuccess + "; money earned: " + earnedMoney + ", tax: " + tax + System.Environment.NewLine +
                "Tip: " + tip;
            AddToAuditMemory(auditRecord, warehouse, restaurantBudget, config);
        }

        public void AuditOperation_PerformOrder(string whoBuyWhat, string itemFromOrder, int countOfItemsFromOrder,
            int itemPrice, DishList dishList, BasicIngridientsList ingridientsList, bool isOrderCorrect,
            int dishPrice, int dishTax, Warehouse warehouse, RestaurantBudget restaurantBudgetClass, Config config)
        {
            string auditRecord;
            if (warehouse.GetItemByName(itemFromOrder).GetItemType() == "ingredient") auditRecord = whoBuyWhat + " -> " +
                itemFromOrder + ", " + countOfItemsFromOrder + ", " + dishPrice + " -> success; " +
                "cost: " + itemPrice + ", tax:" + dishTax;
            else auditRecord = whoBuyWhat + " -> " +
                itemFromOrder + ", " + countOfItemsFromOrder + ", " + dishPrice + " -> success; " +
                "cost: " + itemPrice + ", tax:" + dishTax;
            AddToAuditMemory(auditRecord, warehouse, restaurantBudgetClass, config);
        }
    }
}
