﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class PerformBuyCommand_Buy
    {
        public int MoneyToPayForDish(ClientList clientList, string customerName, DishList dishList, string dishName, Config config)
        {
            int moneyLeft_AfterPaying;
            int moneyToPayForDish = dishList.GetDishPrice(dishName);
            int discount = 0;

            if (clientList.GetIsThirdOrder_AfterOrder(customerName))
            {
                discount = config.every_third_discount;
            }
            moneyToPayForDish = moneyToPayForDish - (moneyToPayForDish * discount) / 100;

            return moneyToPayForDish;
        }

        public int PayedTip(int randomChanceToGiveTip, int randomTipPercentage, int moneyToPayForDish, int moneyLeft_AfterPaying)
        {
            bool isClientWillingToGiveTip = false;
            if (randomChanceToGiveTip <= 50) isClientWillingToGiveTip = true;
            int tipAmount = 0;

            if (isClientWillingToGiveTip)
            {
                int tipPercentage = randomTipPercentage;
                tipAmount = (moneyToPayForDish * tipPercentage) / 100;
            }

            if (moneyLeft_AfterPaying - tipAmount < 0)
            {
                tipAmount = moneyLeft_AfterPaying;
                moneyLeft_AfterPaying = 0;
            }

            return tipAmount;
        }

        public string PayForMeal(ClientList clientList, string customerName, RestaurantBudget restaurantBudgetClass, Config config,
            int tipAmount, int moneyToPayForDish, int moneyLeft_AfterPaying )
        {
            string fundsInfo = "";
            clientList.GetClient(customerName).SetTipAmount(tipAmount);
            restaurantBudgetClass.SetCollectedTips(tipAmount);
            clientList.GetClient(customerName).SetClientFunds(clientList.GetClient(customerName).GetClientFunds() - moneyToPayForDish);
            clientList.GetClient(customerName).SetTipAmount(tipAmount);
            restaurantBudgetClass.SetRestaurantBudget(restaurantBudgetClass.GetRestaurantBudget() + moneyToPayForDish);

            if (clientList.GetIsThirdOrder_AfterOrder(customerName))
                fundsInfo = ", but you earn " + config.every_third_discount + "% discount!\r\nYou will pay: " + moneyToPayForDish;
            fundsInfo += ", bought correctly! Tip payed: " + tipAmount + ". Money left: " + moneyLeft_AfterPaying;
            return fundsInfo;
        }

        public int PayTaxesAndAddToBudget(int moneyToPayForDish, RestaurantBudget restaurantBudgetClass, Config config)
        {
            int taxForDish = new BudgetOperations().Tax(moneyToPayForDish, config);
            new BudgetOperations().AddTaxesToCollected(taxForDish, restaurantBudgetClass);
            int moneyToRestaurantBudget = moneyToPayForDish - taxForDish;
            new BudgetOperations().AddMoneyToRestaurantBudget(moneyToRestaurantBudget, restaurantBudgetClass);
            return taxForDish;
        }
    }
}
