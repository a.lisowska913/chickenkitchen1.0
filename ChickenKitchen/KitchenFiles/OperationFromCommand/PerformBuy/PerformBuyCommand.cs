﻿using System;
using System.Text.RegularExpressions;

namespace ChickenKitchen
{
    public class PerformBuyCommand
    {
        public string PerformOperation_CustomerBuyingDish(string whoBuyWhat, string[] customersAndAllergiesAndFundsFile, DishList dishList,
            int restaurantBudget, RestaurantBudget restaurantBudgetClass, ClientList clientList, BasicIngridientsList basicIngridientList,
            Warehouse warehouse, Config config, int random, int randomChanceToGiveTip, int randomTipPercentage, bool isAuditOn, PerformAuditCommand audit.
            bool isPooled)
        {
            Regex regex = new Regex(@"(\D*), (\D*), (\D*)");
            string customerName = regex.Match(whoBuyWhat).Groups[2].Value;
            string dishName = regex.Match(whoBuyWhat).Groups[3].Value;
            string[] basicIngridientsNameList = basicIngridientList.GetBasticIngridientsNameList();

            Client currentClient = clientList.GetClient(customerName);

            //zmienne wynikowe kolejnych etapów
            string allergyInfo = "";
            string fundsInfo = "";
            string warehouseInfo = "";
            string taxInfo = "";
            string result = "";

            int moneyPossesed_BeforePaying = clientList.GetClientFunds(customerName);
            int moneyToPayForDishWithoutDiscount = dishList.GetDishPrice(dishName);

            //sprawdzenie, czy imię klienta jest poprawne
            bool cusotmerIsCorrect = new CheckIfOrderIsCorrect().CheckCustomer(customerName, clientList);
            if (!cusotmerIsCorrect) return "Incorrect customer!";

            //sprawdzenie, czy nazwa dania jest poprawna
            bool dishIsCorrect = new CheckIfOrderIsCorrect().CheckDish(dishName, dishList);
            if (!dishIsCorrect) return "Incorrect dish!";

            //sprawdzenie czy osoba nie ma alergii na składnik
            string allergyIngridient = new CheckAllergiesForIngredient().CheckIfAllergic(customerName, dishName, clientList,
                dishList, basicIngridientList);

            //odpowiedzi w zależności od alergii
            bool isAllergic = false;
            if (allergyIngridient == "none")
                allergyInfo = "success";
            else
            {
                allergyInfo = "can't order, allergic to: " + allergyIngridient;
                isAllergic = true;
                currentClient.SetIsOrderFail(true);
            }

            //sprawdzanie czy osoba ma wystarczające fundusze
            clientList.GetClientFunds(customerName);
            bool isFundsEnoughToBuyDish = new FundsOperations().IsFundsEnoughToBuyDish
                (customerName, dishName, dishList, clientList, basicIngridientList, customersAndAllergiesAndFundsFile);

            //wynik
            if (isAllergic)
            {
                new PerformBuyCommand_AllergyOperations().DoWhileAllergic(dishName, dishList, customerName, clientList, warehouse, config,
                    random, allergyInfo, restaurantBudget, restaurantBudgetClass);
            }
            else
            {
                //jeśli klient ma dość funduszy -> dokonywany jest zakup
                if (isFundsEnoughToBuyDish)
                {
                    //jeśli w magazynie jest dość składników -> dokonywany jest zakup
                    //odjęcie z magazynów składników, zabranie pieniędzy z konta klienta, dodanie pieniędzy do budżetu restauracji
                    if (new WarehouseOperations().ChangeWarehouseItemsQunatitiesAccordingToOrder
                        (dishName, dishList, customerName, clientList, warehouse, config, random))
                    {
                        int moneyToPayForDish = new PerformBuyCommand_Buy().
                            MoneyToPayForDish(clientList, customerName, dishList, dishName, config);

                        int moneyLeft_AfterPaying = clientList.GetClientFunds(customerName) - moneyToPayForDish;
                        int tipAmount = new PerformBuyCommand_Buy().
                            PayedTip(randomChanceToGiveTip, randomTipPercentage, moneyToPayForDish, moneyLeft_AfterPaying);

                        moneyToPayForDish += tipAmount;
                        moneyLeft_AfterPaying -= tipAmount;


                        //pay for meal
                        new PerformBuyCommand_Buy().
                            PayForMeal(clientList, customerName, restaurantBudgetClass, config, tipAmount, moneyToPayForDish, moneyLeft_AfterPaying);

                        //taxing for dish
                        int taxForDish = new PerformBuyCommand_Buy().
                            PayTaxesAndAddToBudget(moneyToPayForDish, restaurantBudgetClass, config);

                        taxInfo = " Tax: " + taxForDish;
                        warehouseInfo = System.Environment.NewLine + "Sufficient amount in warehouse.";

                        int moneyToRestaurantBudget = moneyToPayForDish - taxForDish;
                        if (isAuditOn) audit.AuditOperation_PerformBuy(whoBuyWhat, customerName, clientList.GetClientFunds(customerName),
                            dishName, dishList.GetDishPrice(dishName), "success", moneyToRestaurantBudget, taxForDish, tipAmount, warehouse,
                            restaurantBudgetClass, config);
                    }
                    //jeśli w magazynie nie ma składników -> zakup zostaje anulowany, składniki wracają do magazynu
                    else
                    {
                        fundsInfo = ", sufficient funds.";
                        warehouseInfo = System.Environment.NewLine +"Amount in warehouse insufficient. Can't order.";
                        new WarehouseOperations().ReturnToWarehouseIngridients(customerName, clientList, warehouse, dishList, random);
                        warehouse.ResetIsEnoughForThisDish();
                        currentClient.SetIsOrderFail(true);
                        if (isAuditOn) audit.AuditOperation_PerformBuy(whoBuyWhat, customerName, clientList.GetClientFunds(customerName),
                            dishName, dishList.GetAbsoluteDishPrice(dishName), "fail", 0, 0, 0, warehouse,
                            restaurantBudgetClass, config);
                    }
                }
                //jeśli klient nie ma dość funduszy -> zakup anolowany
                else
                {
                    fundsInfo = " - can't order, budget " + moneyPossesed_BeforePaying;
                    currentClient.SetIsOrderFail(true);
                    if (isAuditOn) audit.AuditOperation_PerformBuy(whoBuyWhat, customerName, clientList.GetClientFunds(customerName),
                        dishName, dishList.GetAbsoluteDishPrice(dishName), "fail", 0, 0, 0, warehouse,
                        restaurantBudgetClass, config);
                }
                result = customerName + " - " + dishName + ": " + allergyInfo + " You have to pay: " + moneyToPayForDishWithoutDiscount + fundsInfo + taxInfo + warehouseInfo;
            }
            return result;
        }

    }
}
