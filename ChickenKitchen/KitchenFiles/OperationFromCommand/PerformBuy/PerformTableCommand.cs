﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ChickenKitchen
{
    public class PerformTableCommand
    {
        public string PerformOperations_MultipleCustomerBuyingDish(string order, string[] customersAndAllergiesAndFundsFile, DishList dishList,
            int restaurantBudget, RestaurantBudget restaurantBudgetClass, ClientList clientList, BasicIngridientsList basicIngridientsList,
            Warehouse warehouse, Config config, int random, int randomChanceToGiveTip, int randomTipPercentage, bool isAuditOn, PerformAuditCommand audit)
        {
            Regex regex = new Regex(@"(\w+), (\D*\d*)");
            string orderCustomerAndDishes = regex.Match(order).Groups[2].Value;
            string[] orderCustomerAndDishesList = orderCustomerAndDishes.Split(',');
            string[] customerNameList = new string[16];
            string[] dishNameList = new string[16];

            List<Client> listOfClients = clientList.GetClientList();

            int customerNameIterator = 0;
            int dishNameIterator = 0;
            int begginingRestaurantBudget = restaurantBudget;
            int[] customerFundsList = new int[16];

            bool isTableSucces = true;

            string result = "";
            string begginingAuditRecord = order;

            for (int i=0 ; i < orderCustomerAndDishesList.Length; i++)                                      //sprawdzenie czy zamawiający 
            {                                                                                               //i zamawiane danie są poprawne
                orderCustomerAndDishesList[i] = orderCustomerAndDishesList[i].Trim();
                if (new CheckIfOrderIsCorrect().CheckCustomer(orderCustomerAndDishesList[i], clientList))
                {
                    customerNameList[customerNameIterator] = orderCustomerAndDishesList[i];
                    Client currentClient = clientList.GetClient(customerNameList[i]);
                    customerFundsList[customerNameIterator] = currentClient.GetClientFunds();
                    customerNameIterator++;
                }
                else if (new CheckIfOrderIsCorrect().CheckDish(orderCustomerAndDishesList[i], dishList))
                {
                    dishNameList[dishNameIterator] = orderCustomerAndDishesList[i];
                    dishNameIterator++;
                }
                else
                {
                    begginingAuditRecord += " -> failed; money earned: 0, tax: 0";
                    audit.AddToAuditMemory(begginingAuditRecord, warehouse, restaurantBudgetClass, config);
                    return "Order failed!";
                }
            }

            if (isAuditOn)                                                                                      //begining of audit
            {
                begginingAuditRecord += " -> BEGINING";
                audit.AddToAuditMemoryPlain(begginingAuditRecord);
            }

            for (int i = 0; i < customerNameIterator; i++)                                                      //wykonywanie zamówienia dla stolika
            {
                Client currentCustomer = clientList.GetClient(customerNameList[i]);
                if (currentCustomer.GetIsOrderDone()) isTableSucces = false;                                    //czy klient nie zamawiał w tym zamówieniu            

                //wykonaj zamówienie dla pojedynczego klienta
                string orderForSinglePerson = "Buy, " + customerNameList[i] + ", " + dishNameList[i];   
                string currentResult = System.Environment.NewLine + new PerformBuyCommand().PerformOperation_CustomerBuyingDish(orderForSinglePerson,
                    customersAndAllergiesAndFundsFile,dishList, restaurantBudget, restaurantBudgetClass, clientList, basicIngridientsList,
                    warehouse, config, random, randomChanceToGiveTip, randomTipPercentage, isAuditOn, audit, false);

                if (currentResult == "\r\nIncorrect dish!") isTableSucces = false;

                result += currentResult;
                currentCustomer.SetIsOrderDone(true);
            }

            if (dishNameList[customerNameIterator] != null)
            {
                result += System.Environment.NewLine + "Incorrect customer!";
                isTableSucces = false;
            }

            for (int i = 0; i<clientList.GetClienListLength();i++)                                                  //osoba zamawiała w tym zamówieniu
                listOfClients[i].SetIsOrderDone(false);

            for (int i = 0; i < clientList.GetClienListLength(); i++)                                               //czy zamówieni stolika się powiodło
                if (listOfClients[i].GetIsOrderFail()) isTableSucces = false;

            return TableResult(isTableSucces, order, audit, warehouse, config, result, restaurantBudgetClass,       //return results according to success
                customerNameList, clientList, customerFundsList, dishList, begginingRestaurantBudget);
        }

        private string TableResult(bool isTableSuccess, string order, PerformAuditCommand audit, Warehouse warehouse,
            Config config, string result, RestaurantBudget restaurantBudgetClass, string[] customerNameList,
            ClientList clientList, int[] customerFundsList, DishList dishList, int begginingRestaurantBudget)
        {
            if (isTableSuccess)                                                                                      //zwrócenie rezultatu operacji
            {
                string endAuditRecord = order + " ->success; END";
                audit.AddToAuditMemory(endAuditRecord, warehouse, restaurantBudgetClass, config);
                return result;
            }
            else
            {                                                                                                       //return money
                int currentTax = 0;
                for (int i = 0; i < customerNameList.Length; i++)
                {
                    Client currentClient = clientList.GetClient(customerNameList[i]);                               //return money to customer
                    int currentClientcurrFunds = currentClient.GetClientFunds();
                    currentClient.SetClientFunds(customerFundsList[i]);
                    new WarehouseOperations().ReturnToWarehouseDish                                                 //return ingridients to warehouse
                        (customerNameList[i], clientList, warehouse, dishList);
                    restaurantBudgetClass.SetCollectedTax                                                           //remove money from restaurant budget
                        (restaurantBudgetClass.GetCollectedTax() - 1 -
                        ((customerFundsList[i] - currentClientcurrFunds) * config.transaction_tax / 100));
                    currentClient.SetIsOrderFail(false);
                }
                restaurantBudgetClass.SetRestaurantBudget(begginingRestaurantBudget);
                string endAuditRecord = order + " ->failed; END";
                audit.AddToAuditMemory(endAuditRecord, warehouse, restaurantBudgetClass, config);
                return result += System.Environment.NewLine + "Table failed. Money Returned";
            }
        }
    }
}


