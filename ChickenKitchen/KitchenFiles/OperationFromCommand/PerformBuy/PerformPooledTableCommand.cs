﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class PerformPooledTableCommand
    {
        public string Buy(string order, string[] customersAndAllergiesAndFundsFile, DishList dishList,
            int restaurantBudget, RestaurantBudget restaurantBudgetClass, ClientList clientList, BasicIngridientsList basicIngridientsList,
            Warehouse warehouse, Config config, int random, int randomChanceToGiveTip, int randomTipPercentage, bool isAuditOn, PerformAuditCommand audit)
        {
            Regex regex = new Regex(@"(\w+), (\D*\d*)");
            string orderCustomerAndDishes = regex.Match(order).Groups[2].Value;
            string[] orderCustomerAndDishesList = orderCustomerAndDishes.Split(',');
            string[] customerNameList = new string[16];
            string[] dishNameList = new string[16];

            int customerNameIterator = 0;
            int dishNameIterator = 0;
            int begginingRestaurantBudget = restaurantBudget;
            int[] customerFundsList = new int[16];

            bool isTableSucces = true;
            bool isPooled = false;

            string result = "";
            string begginingAuditRecord = order;

            for (int i = 0; i < orderCustomerAndDishesList.Length; i++)                                     //sprawdzenie czy zamawiający 
            {                                                                                               //i zamawiane danie są poprawne
                orderCustomerAndDishesList[i] = orderCustomerAndDishesList[i].Trim();

                if (orderCustomerAndDishesList[i] == "Pooled")                                              //sprawdzenie czy zamówienie jest polowane
                {
                    isPooled = true;
                }
                else if (new CheckIfOrderIsCorrect().CheckCustomer(orderCustomerAndDishesList[i], clientList))
                {
                    customerNameList[customerNameIterator] = orderCustomerAndDishesList[i];
                    Client currentClient = clientList.GetClient(customerNameList[i]);
                    customerFundsList[customerNameIterator] = currentClient.GetClientFunds();
                    customerNameIterator++;
                }
                else if (new CheckIfOrderIsCorrect().CheckDish(orderCustomerAndDishesList[i], dishList))
                {
                    dishNameList[dishNameIterator] = orderCustomerAndDishesList[i];
                    dishNameIterator++;
                }
                else
                {
                    begginingAuditRecord += " -> failed; money earned: 0, tax: 0";
                    audit.AddToAuditMemory(begginingAuditRecord, warehouse, restaurantBudgetClass, config);
                    return "Order failed!";
                }
            }

            if (isAuditOn)                                                                                      //begining of audit
            {
                begginingAuditRecord += " -> BEGINING";
                audit.AddToAuditMemoryPlain(begginingAuditRecord);
            }


            for (int i = 0; i < customerNameIterator; i++)                                                      //wykonywanie zamówienia dla stolika
            {
                Client currentCustomer = clientList.GetClient(customerNameList[i]);
                if (currentCustomer.GetIsOrderDone()) isTableSucces = false;                                    //czy klient nie zamawiał w tym zamówieniu            

                //wykonaj zamówienie dla pojedynczego klienta

                currentCustomer.SetIsOrderDone(true);
            }
        }
        
    }
}
