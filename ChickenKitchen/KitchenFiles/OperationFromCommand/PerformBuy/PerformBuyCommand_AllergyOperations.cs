﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class PerformBuyCommand_AllergyOperations
    {
        public string DoWhileAllergic(string dishName, DishList dishList, string customerName,
            ClientList clientList, Warehouse warehouse, Config config, int random, string allergyInfo,
            int restaurantBudget, RestaurantBudget restaurantBudgetClass)
        {
            //dla osoby z alergią -> składniki przepadają, ale nie ponosi ona kosztów
            new WarehouseOperations().ChangeWarehouseItemsQunatitiesAccordingToOrder
                (dishName, dishList, customerName, clientList, warehouse, config, random);
            string result ="";

            switch (config.dishes_with_allergies)
            {
                case "waste":
                    warehouse.GetItemByName(dishName).ChangeWastedAmount(1);
                    result = customerName + " - " + dishName + ": " + allergyInfo +
                        "\r\nDish wasted!";
                    break;
                case "keep":
                    if (new WarehouseStorage().IsEnoughSpaceInWarehouse(warehouse, config, dishName))
                    {
                        if (restaurantBudget > dishList.GetDishPrice(dishName) / 4)
                        {
                            restaurantBudget = restaurantBudgetClass.GetInitialRestaurantBudget() - dishList.GetDishPrice(dishName) / 4;
                            restaurantBudgetClass.SetRestaurantBudget(restaurantBudget);
                            new WarehouseOperations().ChangeWarehouseItemQuantity(dishName, 1, warehouse);
                            result = customerName + " - " + dishName + ": " + allergyInfo +
                                "\r\nDish keeped!";
                        }
                        else
                        {
                            new WarehouseOperations().ChangeWarehouseItemQuantity(dishName, 1, warehouse);
                            result = customerName + " - " + dishName + ": " + allergyInfo +
                                "\r\nDish wasted!";
                        }
                    }
                    else
                    {
                        warehouse.GetItemByName(dishName).ChangeWastedAmount(1);
                        result = customerName + " - " + dishName + ": " + allergyInfo +
                            "\r\nDish wasted!";
                    }
                    break;
                case string number when Regex.IsMatch(number, @"(\d*)"):
                    if (Int32.Parse(number) > dishList.GetDishPrice(dishName))
                    {
                        warehouse.GetItemByName(dishName).ChangeWastedAmount(1);
                        result = customerName + " - " + dishName + ": " + allergyInfo +
                            "\r\nDish wasted!";
                    }
                    else
                    {
                        if (new WarehouseStorage().IsEnoughSpaceInWarehouse(warehouse, config, dishName))
                        {
                            new WarehouseOperations().ChangeWarehouseItemQuantity(dishName, 1, warehouse);
                            result = customerName + " - " + dishName + ": " + allergyInfo +
                                "\r\nDish keeped!";
                        }
                        else
                        {
                            warehouse.GetItemByName(dishName).ChangeWastedAmount(1);
                            result = customerName + " - " + dishName + ": " + allergyInfo +
                                "\r\nDish wasted!";
                        }
                    }
                    break;
            }
            return result;
        }
    }
}