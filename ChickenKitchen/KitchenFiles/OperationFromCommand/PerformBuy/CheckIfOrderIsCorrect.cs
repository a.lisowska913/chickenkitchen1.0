﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ChickenKitchen
{
    public class CheckIfOrderIsCorrect
    {
        //check if dish is correct
        public bool CheckDish(string dishName, DishList dishList)
        {
            List<Dish> currDishList = dishList.GetDishList();
            Regex regex = new Regex(@"(\D*): (\D*)");

            for (int i=0; i< currDishList.Count;i++)
            {
                if (currDishList[i].GetDishName() == dishName) return true;
            }
            return false;
        }
        //check if customer is correct
        public bool CheckCustomer(string customerName, ClientList clientList)
        {
            Client client = clientList.GetClient(customerName);
            if (client.GetClientName() == "") return false;
            else return true;
        }
    }
}

