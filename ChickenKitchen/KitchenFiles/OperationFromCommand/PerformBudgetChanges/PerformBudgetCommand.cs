﻿using System;
using System.Text.RegularExpressions;

namespace ChickenKitchen
{
    public class PerformBudgetCommand
    {
        public string ChangeBudget(string whoBuyWhat, int restaurantBudget, RestaurantBudget restaurantBudgetClass,
            bool isAuditOn, PerformAuditCommand audit, Warehouse warehouse, Config config)
        {
            Regex regex = new Regex(@"(\D*), (\D*), (\d+)");
            string budgetOperation = regex.Match(whoBuyWhat).Groups[2].Value;
            int value = Int32.Parse(regex.Match(whoBuyWhat).Groups[3].Value);
            string isSuccess = "success; ";

            switch (budgetOperation)
            {
                case "+":
                    new BudgetOperations().AddMoneyToRestaurantBudget(value, restaurantBudgetClass);
                    if (isAuditOn) audit.AuditOperation_PerformBudgetCommand(whoBuyWhat, restaurantBudget, budgetOperation, value,
                        restaurantBudget + value, isSuccess, warehouse, restaurantBudgetClass, config);
                    return "Budget changed! Added: " + value;
                case "-":
                    new BudgetOperations().AddMoneyToRestaurantBudget(-value, restaurantBudgetClass);
                    if (isAuditOn) audit.AuditOperation_PerformBudgetCommand(whoBuyWhat, restaurantBudget, budgetOperation, value,
                        restaurantBudget - value, isSuccess, warehouse, restaurantBudgetClass, config);
                    return "Budget changed! Loses: " + value;
                case "=":
                    restaurantBudgetClass.SetRestaurantBudget(value);
                    if (isAuditOn) audit.AuditOperation_PerformBudgetCommand(whoBuyWhat, restaurantBudget, budgetOperation, value,
                        value, isSuccess, warehouse, restaurantBudgetClass, config);
                    return "Budget changed to: " + value;
                default:
                    if (isAuditOn) audit.AuditOperation_PerformBudgetCommand(whoBuyWhat, restaurantBudget, budgetOperation, value,
                        restaurantBudget, "fail; ", warehouse, restaurantBudgetClass, config);
                    return "Incorrect sign!";
            }
        }

    }
}
