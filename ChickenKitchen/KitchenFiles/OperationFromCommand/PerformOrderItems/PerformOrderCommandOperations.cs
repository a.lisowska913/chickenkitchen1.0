﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class PerformOrderCommandOperations
    {
        public string OrderIngredientsToWarehouse(string whoBuyWhat, int restaurantBudget, RestaurantBudget restaurantBudgetClass,
            BasicIngridientsList basicIngridientsList, DishList dishList, Warehouse warehouse, Config config,
            string itemName, int howManyItems, string order, int randomIngredientVolatility, int randomDishVolatility, PerformAuditCommand audit,
            bool isAuditOn
            )
        {
            string[] itemNameList;
            int itemPrice = 0;
            int itemCost = 0;
            double itemTax = config.transaction_tax;
            double volatility = 0.0;

            int wastedIngridients_exceededMaxTotal = new WarehouseStorage().IsWarehouseFull(warehouse, config);
            int wastedIngridients_exceededMaxIngridients = 0;
            int wastedIngridients_exceededMaxDish = 0;

            //operacja w zależności od tego, czy zamawiany jest składnik czy danie
            if (order == "ingredient")
            {
                volatility = 1.0 + (randomIngredientVolatility / 100.0);
                itemPrice = basicIngridientsList.GetIngridientPriceFromList(itemName);
                itemTax = (itemPrice * itemTax)/100;
                itemCost = System.Convert.ToInt32(howManyItems * (itemPrice + itemTax) * volatility);

                new BudgetOperations().AddMoneyToRestaurantBudget(-itemCost, restaurantBudgetClass);
                new WarehouseOperations().ChangeWarehouseItemQuantity(itemName, howManyItems, warehouse);
                wastedIngridients_exceededMaxIngridients = new WarehouseStorage().IsWarehouseFull(warehouse, config.max_ingridient_type, itemName);
            }
            else
            {
                volatility = 1.0 + (randomDishVolatility / 100.0);
                itemNameList = dishList.GetDishNameList();
                itemPrice = dishList.GetAbsoluteDishPrice(itemName);
                itemTax = (itemPrice * itemTax) / 100;
                itemCost = System.Convert.ToInt32(howManyItems * (itemPrice + itemTax) * volatility);

                new BudgetOperations().AddMoneyToRestaurantBudget(-itemCost, restaurantBudgetClass);
                new WarehouseOperations().ChangeWarehouseItemQuantity(itemName, howManyItems, warehouse);
                wastedIngridients_exceededMaxDish = new WarehouseStorage().IsWarehouseFull(warehouse, config.max_dish_type, itemName);
            }

            warehouse.SetTotalNumberOfItemsInWarehouse(new WarehouseStorage().CountTotalNumberOfItemInWarehouse(warehouse));
            wastedIngridients_exceededMaxTotal = new WarehouseStorage().IsWarehouseFull(warehouse, config);
            string waste = "";

            //sprawdzenie ile zamówienia nie zmieści się do magazynu
            if (wastedIngridients_exceededMaxTotal > wastedIngridients_exceededMaxIngridients ||                //czy zamówienie nie przekracza całkowitej
                wastedIngridients_exceededMaxTotal > wastedIngridients_exceededMaxDish)                         //wielkości magazynu
            {
                waste += wastedIngridientsExceededMaxTotal(itemName, wastedIngridients_exceededMaxTotal,
                    warehouse, wastedIngridients_exceededMaxDish, config, dishList) + "\r\n";
                if (wastedIngridients_exceededMaxIngridients != 0)                                              //czy nie przekracza ilości składników
                    waste += new WasteOperations().CreateWasteRecord(itemName, wastedIngridients_exceededMaxIngridients,
                        warehouse, config.max_ingridient_type, dishList, config, "");
                else if (wastedIngridients_exceededMaxDish != 0)                                                //czy nie przekracza ilości dań
                    waste += new WasteOperations().CreateWasteRecord(itemName, wastedIngridients_exceededMaxDish,
                        warehouse, config.max_dish_type, dishList, config, "");
            }
            else
            {
                if (wastedIngridients_exceededMaxIngridients != 0)
                {
                    waste += new WasteOperations().CreateWasteRecord                                            //czy nie przekracza ilości składników
                        (itemName, wastedIngridients_exceededMaxIngridients,              
                        warehouse, config.max_ingridient_type, dishList, config, "");
                }
                else if (wastedIngridients_exceededMaxDish != 0)                                                //czy nie przekraza ilości dań
                {
                    waste += new WasteOperations().CreateWasteRecord(itemName, wastedIngridients_exceededMaxDish,
                        warehouse, config.max_dish_type, dishList, config, "");
                }
            }

            if (isAuditOn) audit.AuditOperation_PerformOrder(whoBuyWhat, itemName, howManyItems, itemCost, dishList,
                basicIngridientsList, true, itemPrice, Convert.ToInt32(itemTax*howManyItems), warehouse, restaurantBudgetClass, config);
            if (waste != "") return waste + "\r\nCosts of transaction: " + itemCost;
            return "Bought successfuly! It costs: " + itemCost;
        }

        private string wastedIngridientsExceededMaxTotal(string itemName, int wastedIngridients_exceededMaxTotal, Warehouse warehouse,
            int wastedIngridients, Config config, DishList dishList)
        {
            wastedIngridients_exceededMaxTotal -= wastedIngridients;
            if (wastedIngridients_exceededMaxTotal > 0)
            {
                return new WasteOperations().CreateWasteRecord(itemName, wastedIngridients_exceededMaxTotal,
                    warehouse, config.total_maximum, dishList, config, "warehouse ");
            }
            return "";
        }
        
    }
}
