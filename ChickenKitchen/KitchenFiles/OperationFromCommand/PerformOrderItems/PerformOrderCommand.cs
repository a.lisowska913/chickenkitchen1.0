﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ChickenKitchen
{
    public class PerformOrderCommand
    {
        public string OrderToWarehouse(string whoBuyWhat, int restaurantBudget, RestaurantBudget restaurantBudgetClass,
            BasicIngridientsList basicIngridientsList, DishList dishList, Warehouse warehouse, Config config, int random,
            int randomIngredientVolatility, int randomDishVolatility, PerformAuditCommand audit, bool isAuditOn)
        {
            Regex regex = new Regex(@"(\w+), ((\D*\d*)+)");
            string orderedItems = regex.Match(whoBuyWhat).Groups[2].Value;
            string[] orderItemsAndQuantitysList = orderedItems.Split(",");

            List<string> itemsFromOrder = new List<string>();
            List<int> countOfItemsFromOrder = new List<int>();

            string orderLog = "";

            for (int i = 0; i < orderItemsAndQuantitysList.Length; i++)                                 //segreguje itemy i ilość zamówienia
            {
                if (i % 2 == 0) itemsFromOrder.Add(orderItemsAndQuantitysList[i]);
                if (i % 2 == 1) countOfItemsFromOrder.Add(Int32.Parse(orderItemsAndQuantitysList[i]));
            }

            int isOrderCorrect = IsOrderCorrect(itemsFromOrder, basicIngridientsList, dishList, config);//sprawdza, czy wszystkie elementy zamówienia
            switch (isOrderCorrect)                                                                     //są w porządku
            {
                case 0: break;
                case 1:
                    if(isAuditOn) audit.AddToAuditMemory(MakeErrorAuditRecord(whoBuyWhat), warehouse, restaurantBudgetClass, config);
                    return "Incorrect order!";
                case 2:
                    if (isAuditOn) audit.AddToAuditMemory(MakeErrorAuditRecord(whoBuyWhat), warehouse, restaurantBudgetClass, config);
                    return "Incorrect ingridient!";
                case 3:
                    if (isAuditOn) audit.AddToAuditMemory(MakeErrorAuditRecord(whoBuyWhat), warehouse, restaurantBudgetClass, config);
                    return "Incorrect dish!";
            }

            if (isAuditOn) audit.AddToAuditMemoryPlain(whoBuyWhat);
            for (int i = 0; i < itemsFromOrder.Count; i++)                                              //wykonanie zamówienia
            {
                itemsFromOrder[i] = itemsFromOrder[i].Trim();
                string order = CheckWhatIsOrdered(itemsFromOrder[i], basicIngridientsList, dishList);

                orderLog += new PerformOrderCommandOperations().OrderIngredientsToWarehouse
                            (whoBuyWhat, restaurantBudget, restaurantBudgetClass, basicIngridientsList,
                            dishList, warehouse, config, itemsFromOrder[i], countOfItemsFromOrder[i],
                            order, randomIngredientVolatility, randomDishVolatility, audit, isAuditOn) + "\r\n";
            }
            new WarehouseSpoiling().CheckIfIngredientsArentSpoiled(warehouse, config, random);                  //sprawdzenie, czy itemy nie spelśniały
            return orderLog + "\r\nOrder end.";
        }

        private string MakeErrorAuditRecord(string whoBuyWhat)
        {
            string auditRecord = whoBuyWhat + " -> fail; 0, 0";
            return auditRecord;
        }

        public string CheckWhatIsOrdered(string ingridientName, BasicIngridientsList basicIngridientsList, DishList dishList)
        {
            if (basicIngridientsList.IsBasicIngridient(ingridientName)) return "ingredient";
            else if (dishList.IsDish(ingridientName)) return "dish";
            else return "Incorrect order!";
        }

        private int IsOrderCorrect(List<string> itemsFromOrder, BasicIngridientsList basicIngridientsList, DishList dishList, Config config)
        {
            for (int i = 0; i < itemsFromOrder.Count; i++)
            {
                itemsFromOrder[i] = itemsFromOrder[i].Trim();
                string order = CheckWhatIsOrdered(itemsFromOrder[i], basicIngridientsList, dishList);
                if (order == "Incorrect order!") return 1;

                switch (config.order)
                {
                    case "ingredient":
                        if (order != "ingredient") return 2;
                        break;
                    case "dish":
                        if (order != "dish") return 3;
                        break;
                    case "all": break;
                }
            }
            return 0;
        }
    }
}
