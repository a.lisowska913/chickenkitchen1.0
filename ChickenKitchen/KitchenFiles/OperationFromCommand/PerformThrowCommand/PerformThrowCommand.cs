﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class PerformThrowCommand
    {
        public string Throw(Warehouse warehouse, bool isAuditOn, PerformAuditCommand audit, RestaurantBudget restaurantBudget, Config config)
        {
            warehouse.SetThrash(0);
            string thrownWasteRecord = CreateThrownWasteRecord(warehouse);
            new SaveToFile().SaveToWasteFile(thrownWasteRecord);
            if (isAuditOn) audit.AddToAuditMemory(MakeAuditRecord(thrownWasteRecord), warehouse, restaurantBudget, config);
            return thrownWasteRecord;
        }
        private string CreateThrownWasteRecord(Warehouse warehouse)
        {
            List<WasteItem> wasteItemList = warehouse.GetWarehouseWasteList();
            string record = "Waste pool: {";
            for (int i=0;i<wasteItemList.Count;i++)
            {
                if (wasteItemList[i].GetItemQuantity() > 0)
                {
                    record += wasteItemList[i].GetItemName() + ": " + wasteItemList[i].GetItemQuantity() + ", ";
                }
            }
            return record += "}";
        }
        private string MakeAuditRecord(string thrownWasteRecord)
        {
            string auditRecord =  "Throw trash away -> success; ";
            return auditRecord;
        }
    }
}
