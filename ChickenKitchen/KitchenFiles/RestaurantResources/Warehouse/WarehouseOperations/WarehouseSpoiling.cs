﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class WarehouseSpoiling
    {
        public int CheckIfIngredientsArentSpoiled(Warehouse warehouse, Config config, int random)
        {
            List<WarehouseItem> warehouseItems = warehouse.GetWarehouseItemList();
            int quantityOfSpoiledIngredients = 0;

            for (int i=0;i<warehouseItems.Count;i++)
            {
                if (warehouseItems[i].GetItemType() == "ingredient")
                {
                    for (int j = 0; j < warehouseItems[i].GetItemQuantities(); j++)
                    {
                        bool isSpoiled = CheckIfIsSpoiled(warehouseItems[i], config, random);
                        if (isSpoiled == true)
                        {
                            warehouse.GetWasteItemByName(warehouseItems[i].GetItemName()).SetItemQuantity(
                                warehouse.GetWasteItemByName(warehouseItems[i].GetItemName()).GetItemQuantity() + 1);
                            quantityOfSpoiledIngredients++;
                        }
                        warehouse.SetThrash(warehouse.GetThrashAmount() + 1);
                    }
                }
            }
            return quantityOfSpoiledIngredients;
        }

        private bool CheckIfIsSpoiled(WarehouseItem warehouseItem, Config config, int random)
        {
            double chance = config.spoil_rate*10;
            if (random <= chance)
            {
                warehouseItem.SetItemQuantities(warehouseItem.GetItemQuantities() - 1);
                return true;
            }
            return false;
        }
    }
}
