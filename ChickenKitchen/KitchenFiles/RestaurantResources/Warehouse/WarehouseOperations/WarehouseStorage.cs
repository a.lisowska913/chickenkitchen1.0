﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class WarehouseStorage
    {
        public int CountTotalNumberOfItemInWarehouse (Warehouse warehouse)
        {
            int totalNumberOfItemsInWarehouse = 0;
            List<WarehouseItem> warehouseList = warehouse.GetWarehouseItemList();
            for (int i =0; i< warehouseList.Count;i++)
            {
                totalNumberOfItemsInWarehouse += warehouseList[i].GetItemQuantities();
            }
            return totalNumberOfItemsInWarehouse;
        } 
        public int IsWarehouseFull (Warehouse warehouse, Config config)
        {
            if (warehouse.GetTotalNumberOfItemsInWarehouse() > config.total_maximum)
            {
                int wastedIngridient = CountTotalNumberOfItemInWarehouse(warehouse) - config.total_maximum;    
                return wastedIngridient;
            }
            else return 0;
        }
        public int IsWarehouseFull (Warehouse warehouse, int maxValue, string dishName)
        {
            if (warehouse.GetItemByName(dishName).GetItemQuantities() > maxValue)
            {
                return warehouse.GetItemByName(dishName).GetItemQuantities() - maxValue;
            }
            else return 0;
        }
        public bool IsEnoughSpaceInWarehouse(Warehouse warehouse, Config config, string itemName)
        {
            if (IsWarehouseFull(warehouse, config) != 0) return false;
            if (warehouse.GetItemByName(itemName).GetItemType() == "dish")
            {
                if (IsWarehouseFull(warehouse, config.max_dish_type, itemName) != 0) return false;
            }
            else if (IsWarehouseFull(warehouse, config.max_ingridient_type, itemName) != 0) return false;
            return true;
        }
        public string GetWastedTotalLog(Warehouse warehouse, Config config)
        {
            List<WarehouseItem> warehouseList = warehouse.GetWarehouseItemList();
            string wastedLog = "Wasted: ";
            for (int i =0; i<warehouseList.Count;i++)
            {
                if (warehouseList[i].GetWastedAmount() != 0)
                {
                    if (warehouseList[i].GetItemType() == "ingredient")
                        wastedLog += warehouseList[i].GetWastedAmount() + " " + warehouseList[i].GetItemName() +
                        " (limit: " + config.max_ingridient_type + "), ";
                    else
                        wastedLog += warehouseList[i].GetWastedAmount() + " " + warehouseList[i].GetItemName() +
                        " (limit: " + config.max_dish_type + "), ";
                }
            }
            return wastedLog;
        }
    }
}
