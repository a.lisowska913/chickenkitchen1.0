﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    class WarehouseOperations
    {
        bool isEnoughIngredients = true;

        public void ReturnToWarehouseIngridients(string customerName, ClientList clientList, Warehouse warehouse, DishList dishList, int random)
        {
            List<string> itemsToReturn = clientList.GetClient(customerName).GetOrder();
            for (int i = 0; i < itemsToReturn.Count; i++)
            {
                if (warehouse.GetItemByName(itemsToReturn[i]).GetItemType() == "dish")
                {
                    List<string> ingridients = dishList.GetDishIngridientsAll(itemsToReturn[i]);
                    for (int j=0;j<ingridients.Count;j++)
                    {
                        if(warehouse.GetItemByName(ingridients[j]).GetIsEnoughForThisDish() == true)
                            ChangeWarehouseItemQuantity(ingridients[j], 1, warehouse);
                    }
                }
                else ChangeWarehouseItemQuantity(itemsToReturn[i], 1, warehouse);
            }
        }

        public void ReturnToWarehouseDish(string customerName, ClientList clientList, Warehouse warehouse, DishList dishList)
        {
            List<string> itemsToReturn = clientList.GetClient(customerName).GetOrder();
            for (int i = 0; i < itemsToReturn.Count; i++)
            {
                ChangeWarehouseItemQuantity(itemsToReturn[i], 1, warehouse);
            }
        }

        public bool ChangeWarehouseItemsQunatitiesAccordingToOrder
            (string dishName, DishList dishList, string customerName, ClientList clientList, Warehouse warehouse, Config config, int random)
        {
            WarehouseItem currentItem = warehouse.GetItemByName(dishName);
            List<string> dishes = new List<string>();
            dishes.Add(dishName);

            if (currentItem.GetItemQuantities() > 0)                                                //jeśli danie jest, wydaj je klientowi
            {
                currentItem.ChangeItemQuantities(-1);
                clientList.GetClient(customerName).SetOrder(dishes);
                new WarehouseSpoiling().CheckIfIngredientsArentSpoiled(warehouse, config, random);
                return true;
            }
            else
            {                                                                                       //jeśli dania nie ma, zrób z subdań
                List<string> notPreparedSubdish = new List<string>();

                notPreparedSubdish = ChangeItemQuantitiesInCurrentLayer
                        (warehouse, currentItem, customerName, clientList, dishList, dishName, dishes);

                while (notPreparedSubdish.Count != 0)
                {
                    for (int i = 0; i < notPreparedSubdish.Count; i++)
                    {
                        string itemName = notPreparedSubdish[i];
                        notPreparedSubdish = ChangeItemQuantitiesInCurrentLayer
                            (warehouse, currentItem, customerName, clientList, dishList, itemName, dishes);
                    }
                }

                new WarehouseSpoiling().CheckIfIngredientsArentSpoiled(warehouse, config, random);
                return isEnoughIngredients;
            }
        }

        public List<string> ChangeItemQuantitiesInCurrentLayer(Warehouse warehouse, WarehouseItem currentItem,
            string customerName, ClientList clientList, DishList dishList, string itemName,
            List<string> dishes)
        {
            List<string> subdishInDish = dishList.GetDishSubdish(itemName);
            List<string> notPreparedSubdishes = new List<string>();

            List<string> ingredientsInDish = dishList.GetDishIngridients(itemName);

            for (int i = 0; i < subdishInDish.Count; i++)                                       //czy jest dość subdań w magazynie
            {
                currentItem = warehouse.GetItemByName(subdishInDish[i]);
                if (currentItem.GetItemQuantities() > 0)                                        //jeśli tak wydaj klientowi
                {
                    currentItem.ChangeItemQuantities(-1);
                    clientList.GetClient(customerName).SetOrder(dishes);
                }
                else
                {                                                                               //jesli nie, wpisz na listę brakujących
                    notPreparedSubdishes.Add(currentItem.GetItemName());
                    currentItem.SetIsEnoughForThisDish(false);
                }
            }

            for (int i =0;i<ingredientsInDish.Count;i++)                                        //czy jest dość składników
            {
                currentItem = warehouse.GetItemByName(ingredientsInDish[i]);
                if (currentItem.GetItemQuantities() > 0)                                        //jeśli tak wydaj klientowi
                {
                    currentItem.ChangeItemQuantities(-1);
                    clientList.GetClient(customerName).SetOrder(dishes);
                }
                else
                {                                                                               //jesli nie, zwróć false
                    currentItem.SetIsEnoughForThisDish(false);
                    isEnoughIngredients = false;
                }
            }
        return notPreparedSubdishes;
        }

        public void ChangeWarehouseItemQuantity(string itemName, int itemQuantity, Warehouse warehouse)
        {
            List<WarehouseItem> warehouseItemList = warehouse.GetWarehouseItemList();
            for (int i = 0; i < warehouseItemList.Count; i++)
            {
                if (warehouseItemList[i].GetItemName() == itemName)
                {
                    int newQuantity = itemQuantity + warehouseItemList[i].GetItemQuantities();
                    warehouseItemList[i].SetItemQuantities(newQuantity);
                }
            }
        }
    }
}
