﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    class WasteOperations
    {
        public string CreateWasteRecord(string itemName, int wastedIngridients_exceededMax,
            Warehouse warehouse, int maxItemType, DishList dishList, Config config, string isWarehouse)
        {
            new WarehouseOperations().ChangeWarehouseItemQuantity(itemName, -wastedIngridients_exceededMax, warehouse);
            if (warehouse.GetItemByName(itemName).GetItemType() == "dish")
            {
                int amountOfTrash = dishList.GetDishIngridientsAll(itemName).Count;
                warehouse.SetThrash(warehouse.GetThrashAmount() + wastedIngridients_exceededMax * amountOfTrash);
            }
            else
                warehouse.SetThrash(warehouse.GetThrashAmount() + wastedIngridients_exceededMax);

            warehouse.GetItemByName(itemName).ChangeWastedAmount(wastedIngridients_exceededMax);
            WasteItem wasteItem = warehouse.GetWasteItemByName(itemName);
            int currentWasteQuantity = wasteItem.GetItemQuantity();
            int itemsToAdd = currentWasteQuantity + wastedIngridients_exceededMax;
            wasteItem.SetItemQuantity(itemsToAdd);

            if (warehouse.GetThrashAmount() > config.waste_limit)
                return "Wasted: " + wastedIngridients_exceededMax + " " + itemName + " (" + isWarehouse + "limit: " + maxItemType + ")" +
                    "Thrash is full! You have poisoned the kitchen!";
            else
                return "Wasted: " + wastedIngridients_exceededMax + " " + itemName + " (" + isWarehouse + "limit: " + maxItemType + ")";
        }
    }
}
