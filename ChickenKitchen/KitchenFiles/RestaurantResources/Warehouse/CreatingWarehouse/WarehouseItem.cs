﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class WarehouseItem
    {
        string _itemName;
        int _itemQuantities;
        string _typeOfItem;
        int _wastedAmount;
        bool _isEnoughForThisDish;

        public WarehouseItem()
        {
            _itemName = "";
            _itemQuantities = 0;
            _typeOfItem = "none";
            _wastedAmount = 0;
            _isEnoughForThisDish = true;
        }
        public WarehouseItem(string itemName, int itemQuantities, string typeOfItem)
        {
            _itemName = itemName;
            _itemQuantities = itemQuantities;
            _typeOfItem = typeOfItem;
            _wastedAmount = 0;
            _isEnoughForThisDish = true;
        }
        public string GetItemName() { return _itemName; }
        public int GetItemQuantities() { return _itemQuantities; }
        public void SetItemName(string itemName) { _itemName = itemName; }
        public void SetItemQuantities(int itemQuantities) { _itemQuantities = itemQuantities; }
        public void ChangeItemQuantities(int value) { _itemQuantities += value; }
        public int GetWastedAmount() { return _wastedAmount; }
        public void ChangeWastedAmount(int value) { _wastedAmount += value; }
        public string GetItemType() { return _typeOfItem; }
        public bool GetIsEnoughForThisDish() { return _isEnoughForThisDish; }
        public void SetIsEnoughForThisDish(bool isEnough) { _isEnoughForThisDish = isEnough;}
    }
}
