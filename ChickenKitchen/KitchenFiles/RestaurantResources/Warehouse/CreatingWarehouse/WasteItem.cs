﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class WasteItem
    {
        string _itemName;
        int _itemQunatity;

        public WasteItem(string itemName)
        {
            _itemName = itemName;
            _itemQunatity = 0;
        }
        public string GetItemName() { return _itemName; }
        public void SetItemName(string itemName) { _itemName = itemName; }
        public int GetItemQuantity() { return _itemQunatity; }
        public void SetItemQuantity(int value) { _itemQunatity = value; }
    }
}
