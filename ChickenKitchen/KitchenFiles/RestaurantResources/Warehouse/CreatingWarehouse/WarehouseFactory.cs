﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class WarehouseFactory
    {
        public Warehouse CreateInitialWarehouse(BasicIngridientsList basicIngridients, DishList dishList, string warehouseFile)
        {
            List<Ingridient> warehouseBasicIngridientsList = basicIngridients.GetBasicIngridientList();
            List<Dish> warehouseDishList = dishList.GetDishList();
            Warehouse warehouse;

            if (warehouseFile != "File does not exist")
            {
                warehouse = SetInitialQuantitiesInWarehouse(0, 0, warehouseBasicIngridientsList, warehouseDishList);
                warehouse.SetTotalNumberOfItemsInWarehouse(new WarehouseStorage().CountTotalNumberOfItemInWarehouse(warehouse));
                warehouse = CreateWarehouseFormFile(warehouseFile, warehouse);
                warehouse.SetWarehouseWasteList(CreateInitialWaste(warehouse));
                return warehouse;
            }
            else
            {
                warehouse = SetInitialQuantitiesInWarehouse(5, 0, warehouseBasicIngridientsList, warehouseDishList);
                warehouse.SetTotalNumberOfItemsInWarehouse(new WarehouseStorage().CountTotalNumberOfItemInWarehouse(warehouse));
                warehouse.SetWarehouseWasteList(CreateInitialWaste(warehouse));
                return warehouse;
            }
        }
        private Warehouse SetInitialQuantitiesInWarehouse(int quantityOfBasicIngridients, int quantityOfDishes, List<Ingridient> warehouseBasicIngridientsList,
            List<Dish> warehouseDishList)
        {
            List<WarehouseItem> warehouse = new List<WarehouseItem>();

            for (int i = 0; i < warehouseBasicIngridientsList.Count; i++)
            {
                warehouse.Add(
                    new WarehouseItem(warehouseBasicIngridientsList[i].GetIngridientName(), quantityOfBasicIngridients, "ingredient"));
            }
            for (int i = 0; i < warehouseDishList.Count; i++)
            {
                warehouse.Add(
                    new WarehouseItem(warehouseDishList[i].GetDishName(), quantityOfDishes, "dish"));
            }
            return new Warehouse(warehouse);
        }

        private Warehouse CreateWarehouseFormFile(string warehouseFile, Warehouse warehouse)
        {
            string currentWarehouseRecord = "";
            int currentWarehouseQuantity;

            string[] currWarehouse = warehouseFile.Split(",");

            for (int i = 0; i < currWarehouse.Length; i++)
            {
                currWarehouse[i] = currWarehouse[i].Trim();
                if (i % 2 == 0)
                {
                    currentWarehouseRecord = currWarehouse[i];
                }
                else
                {
                    currentWarehouseQuantity = Int32.Parse(currWarehouse[i]);
                    warehouse.SetWarehouseItemQuantity(currentWarehouseRecord, currentWarehouseQuantity);
                    warehouse.SetTotalNumberOfItemsInWarehouse(new WarehouseStorage().CountTotalNumberOfItemInWarehouse(warehouse));
                }
            }
            warehouse.SetTotalNumberOfItemsInWarehouse(new WarehouseStorage().CountTotalNumberOfItemInWarehouse(warehouse));
            return warehouse;
        }
        private List<WasteItem> CreateInitialWaste(Warehouse warehouse)
        {
            List<WarehouseItem> warehouseItems = warehouse.GetWarehouseItemList();
            List<WasteItem> wasteList = new List<WasteItem>();

            for(int i=0;i<warehouseItems.Count;i++)
            {
                wasteList.Add(new WasteItem(warehouseItems[i].GetItemName()));
            }
            return wasteList;
        }
    }
}
