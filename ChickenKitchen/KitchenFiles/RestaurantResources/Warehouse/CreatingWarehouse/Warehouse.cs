﻿using System;
using System.Collections.Generic;

namespace ChickenKitchen
{
    public class Warehouse
    {
        List<WarehouseItem> _warehouse;
        List<WasteItem> _warehouseWaste;

        int _trash; 
        int _totalNumberOfItemsInWarehouse;

        public Warehouse(List<WarehouseItem> warehouse)
        {
            _warehouse = warehouse;
        }
        public List<WarehouseItem> GetWarehouseItemList() { return _warehouse; }
        public int GetWarehouseItemQuantity(string itemName)
        {
            for (int i = 0; i < _warehouse.Count; i++)
            {
                if (_warehouse[i].GetItemName() == itemName)
                {
                    return _warehouse[i].GetItemQuantities();
                }
            }
            return 0;
        }
        public void SetWarehouseItemQuantity(string itemName, int itemQuantity)
        {
            for (int i = 0; i < _warehouse.Count; i++)
            {
                if (_warehouse[i].GetItemName() == itemName)
                {
                    _warehouse[i].SetItemQuantities(itemQuantity);
                }
            }
        }
        public WarehouseItem GetItemByName(string itemName)
        {
            for (int i = 0; i < _warehouse.Count; i++)
            {
                if (itemName == _warehouse[i].GetItemName()) return _warehouse[i];
            }
            return new WarehouseItem();
        } 
        public string GetWarehouseRecord()
        {
            string result = "";
            for (int i =0;i<_warehouse.Count;i++)
            {
                result += _warehouse[i].GetItemName() + ", " + _warehouse[i].GetItemQuantities() + ", ";
            }
            return result;
        }
        public void ResetIsEnoughForThisDish()
        {
            for (int i = 0; i < _warehouse.Count; i++)
            {
                _warehouse[i].SetIsEnoughForThisDish(true);
            }
        }
        public void SetTotalNumberOfItemsInWarehouse(int value) { _totalNumberOfItemsInWarehouse = value; }
        public int GetTotalNumberOfItemsInWarehouse() { return _totalNumberOfItemsInWarehouse; }


        public List<WasteItem> GetWarehouseWasteList() { return _warehouseWaste; }
        public void SetWarehouseWasteList(List<WasteItem> wasteList) { _warehouseWaste = wasteList; }
        public WasteItem GetWasteItemByName(string name)
        {
            for (int i = 0; i < _warehouseWaste.Count; i++)
            {
                if (_warehouseWaste[i].GetItemName() == name) return _warehouseWaste[i];
            }
            return new WasteItem("");
        }
        public int GetWasteTotalCost(BasicIngridientsList basicIngridientsList, DishList dishList, Config config)
        {
            int totalCost = 0;
            for (int i = 0; i < _warehouseWaste.Count; i++)
            {
                string itemName = _warehouseWaste[i].GetItemName();
                if (_warehouse[i].GetItemType() == "ingredient")
                {
                    totalCost += basicIngridientsList.GetIngridientPriceFromList(itemName)*_warehouseWaste[i].GetItemQuantity();
                }
                else
                {
                    int itemPrice = dishList.GetAbsoluteDishPrice(itemName) * _warehouseWaste[i].GetItemQuantity();
                    totalCost += itemPrice;
                }
            }
            return totalCost;
        }
        public void SetThrash(int value) { _trash = value; }
        public int GetThrashAmount() { return _trash; }

        //porównywanie obiektów
        public bool Equal(object? obj)
        {
            if (obj is not Warehouse) return false;

            Warehouse other = obj as Warehouse;
            if (this._warehouse != other._warehouse) return false;
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(this._warehouse);
        }
    }
}
