﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class BudgetOperations
    {
        public int Tax(int dishPrice, Config config)
        {
            int tax = (dishPrice * config.transaction_tax) / 100;
            return tax;
        }
        public void AddTaxesToCollected(int value, RestaurantBudget restaurantBudget)
        {
            restaurantBudget.SetCollectedTax(restaurantBudget.GetCollectedTax() + value);
        }
        public int DailyTax(Config config, RestaurantBudget restaurantBudget)
        {
            int profit = restaurantBudget.GetRestaurantBudget() - restaurantBudget.GetInitialRestaurantBudget() - restaurantBudget.GetCollectedTax();
            int dailyTax = (profit * config.daily_tax) / 100;
            if (dailyTax < 0) return 0;
            else
            {
                restaurantBudget.SetRestaurantBudget(restaurantBudget.GetRestaurantBudget() - dailyTax);
                return dailyTax;
            }
        }
        public void AddMoneyToRestaurantBudget(int value, RestaurantBudget restaurantBudget)
        {
            restaurantBudget.SetRestaurantBudget(restaurantBudget.GetRestaurantBudget() + value);
        }
    }
}
