﻿using System;

namespace ChickenKitchen
{
    public class RestaurantBudget
    {
        int _initialBudgetValue;
        int _budgetValue;
        int _collectedTax;
        int _tipCollected;

        public RestaurantBudget()
        {
            _budgetValue = 0;
            _initialBudgetValue = _budgetValue;
            _collectedTax = 0;
            _tipCollected = 0;
        }
        public RestaurantBudget(int value)
        {
            _budgetValue = value;
            _initialBudgetValue = _budgetValue;
            _collectedTax = 0;
            _tipCollected = 0;
        }
        public int GetBudgetValueFromFile(string[] restaurantBudgetFile)
        {
            string[] budget = restaurantBudgetFile;
            int budgetValue = Int32.Parse(budget[0]);
            return budgetValue;
        }
        public int GetInitialRestaurantBudget() { return _initialBudgetValue;  }
        public void SetRestaurantBudget(int value) { _budgetValue = value; }
        public int GetRestaurantBudget() { return _budgetValue; }
        public void SetCollectedTax(int collectedTax) { _collectedTax = collectedTax;  }
        public int GetCollectedTax() { return _collectedTax; }
        public void SetCollectedTips(int value) { _tipCollected += value; }
        public int GetCollectedTips() { return _tipCollected; }
    }
}