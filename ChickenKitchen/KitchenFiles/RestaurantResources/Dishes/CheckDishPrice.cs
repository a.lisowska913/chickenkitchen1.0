﻿using System;
using System.Collections.Generic;

namespace ChickenKitchen
{
    public class CheckDishPrice
    {
        public int Check(string dishName, string[] foodAndIngredients, BasicIngridientsList basicIngridientsList, Config config)
        {
            List<string> ingridientsList = new AgregateIngridientsFromDishList().AgregateIngridientsFromList(dishName, foodAndIngredients, basicIngridientsList);
            int sum = 0;
            double murkUp = config.profit_margin;

            foreach (string line in ingridientsList)
            {
                sum += basicIngridientsList.GetIngridientPriceFromList(line);
            }

            int markupedValue = Convert.ToInt32(sum * (1+murkUp/100));
            return markupedValue;
        }

        public int CheckAbsolute(string dishName, string[] foodAndIngredients, BasicIngridientsList basicIngridientsList, Config config)
        {
            List<string> ingridientsList = new AgregateIngridientsFromDishList().AgregateIngridientsFromList(dishName, foodAndIngredients, basicIngridientsList);
            int sum = 0;

            foreach (string line in ingridientsList)
            {
                sum += basicIngridientsList.GetIngridientPriceFromList(line);
            }
            return sum;
        }
    }
}

