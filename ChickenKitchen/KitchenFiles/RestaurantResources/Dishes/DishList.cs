﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ChickenKitchen
{
    public class DishList
    {
        List<Dish> _dishList;

        public DishList(List<Dish> dishList)
        {
            _dishList = dishList;
        }
        public List<Dish> GetDishList() { return _dishList; }

        public List<string> GetDishIngridientsAll(string dishName)
        {
            for (int i =0; i<_dishList.Count;i++)
            {
                if (_dishList[i].GetDishName() == dishName) return _dishList[i].GetDishIngridientsAll();
            }
            return new List<string>() { "none" };
        }

        public List<string> GetDishSubdish(string dishName)
        {
            for (int i = 0; i < _dishList.Count; i++)
            {
                if (_dishList[i].GetDishName() == dishName) return _dishList[i].GetDishSubdish();
            }
            return new List<string>() { "none" };
        }

        public List<string> GetDishIngridients(string dishName)
        {
            for (int i = 0; i < _dishList.Count; i++)
            {
                if (_dishList[i].GetDishName() == dishName) return _dishList[i].GetDishIngredients();
            }
            return new List<string>() { "none" };
        }

        public int GetDishPrice(string dishName)
        {
            for (int i = 0; i < _dishList.Count; i++)
            {
                if (_dishList[i].GetDishName() == dishName) return _dishList[i].GetDishPrice();
            }
            return 0;
        }
        public int GetAbsoluteDishPrice(string dishName)
        {
            for (int i = 0; i < _dishList.Count; i++)
            {
                if (_dishList[i].GetDishName() == dishName) return _dishList[i].GetAbsoluteDishPrice();
            }
            return 0;
        }

        public string[] GetDishNameList()
        {
            string[] dishNameList = new string[16];
            for(int i=0;i<_dishList.Count;i++)
            {
                dishNameList[i] = _dishList[i].GetDishName();
            }
            return dishNameList;
        }

        public bool IsDish(string itemName)
        {
            for (int i = 0; i < _dishList.Count; i++)
            {
                if (_dishList[i].GetDishName() == itemName) return true;
            }
            return false;
        }

        //porównywanie obiektów
        public bool Equal(object? obj)
        {
            if (obj is not DishList) return false;

            DishList other = obj as DishList;
            if (this._dishList != other._dishList) return false;
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(this._dishList);
        }
    }
}
