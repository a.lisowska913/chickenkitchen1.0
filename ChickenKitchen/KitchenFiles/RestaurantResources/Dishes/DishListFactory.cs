﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class DishListFactory
    {
        public DishList CreateDishList(string[] foodAndIngridientsFile, BasicIngridientsList basicIngridientList, Config config)
        {
            string currentDishName;
            List<string> currentDishSubdishes = new List<string>();
            List<string> currentDishIngredients = new List<string>();
            List<string> currentDishIngredientsAll;
            List<Dish> currentDishList = new List<Dish>();

            foreach (string line in foodAndIngridientsFile)
            {
                Regex regex = new Regex(@"(\D*): (\D*)");
                currentDishName = regex.Match(line).Groups[1].Value;
                if (currentDishName == "") break;
                currentDishIngredientsAll = new AgregateIngridientsFromDishList().AgregateIngridientsFromList(currentDishName, foodAndIngridientsFile, basicIngridientList);
                int dishPrice = new CheckDishPrice().Check(currentDishName, foodAndIngridientsFile, basicIngridientList, config);
                int absoluteDishPrice = new CheckDishPrice().CheckAbsolute(currentDishName, foodAndIngridientsFile, basicIngridientList, config);
                Dish dish = new Dish(currentDishName, currentDishIngredientsAll, currentDishSubdishes, dishPrice, absoluteDishPrice, currentDishIngredients);
                currentDishList.Add(dish);
            }
            DishList dishList = new DishList(currentDishList);
            CreateSubdishLists(foodAndIngridientsFile, basicIngridientList, config, dishList);
            CreateIngridientsList(foodAndIngridientsFile, basicIngridientList, config, dishList);
            return dishList;
        }

        private void CreateSubdishLists(string[] foodAndIngridientsFile, BasicIngridientsList basicIngridientList, Config config, DishList dishList)
        {
            List<Dish> dishObjectsList = dishList.GetDishList();
            for (int i=0;i<dishObjectsList.Count;i++)
            {
                string currentDishName = dishObjectsList[i].GetDishName();
                List<string> currentDishSubdishes = new AgregateIngridientsFromDishList().AgregateSubdishesFromList
                    (currentDishName, foodAndIngridientsFile, dishList);
                dishObjectsList[i].SetDishSubdishes(currentDishSubdishes);
            }
        }

        private void CreateIngridientsList(string[] foodAndIngridientsFile, BasicIngridientsList basicIngridientList, Config config, DishList dishList)
        {
            List<Dish> dishObjectsList = dishList.GetDishList();
            for (int i = 0; i < dishObjectsList.Count; i++)
            {
                string currentDishName = dishObjectsList[i].GetDishName();
                List<string> currentDishSubdishes = new AgregateIngridientsFromDishList().AgregateLeftIngredientsInThisLayer
                    (currentDishName, foodAndIngridientsFile, dishList, basicIngridientList);
                dishObjectsList[i].SetDishIngredients(currentDishSubdishes);
            }
        }
    }
}
