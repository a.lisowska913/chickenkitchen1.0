﻿using System.Collections.Generic;

namespace ChickenKitchen
{
    public class Dish
    {
        string _dishName;
        int _dishPrice;
        int _absoluteDishPrice;

        List<string> _dishIngredientsAll;

        List<string> _dishSubdishes;
        List<string> _dishIngredients;

        public Dish()
        {
            _dishName = "";
            _dishPrice = 0;
            _absoluteDishPrice = 0;

            _dishIngredientsAll = new List<string>();
            _dishSubdishes = new List<string>();
            _dishIngredients = new List<string>();
        }
        public Dish(string dishName, List<string> dishIngridientsAll, List<string> dishSubdishes, int dishPrice, int absoluteDishPrice, List<string> dishIngredients)
        {
            _dishName = dishName;
            _dishPrice = dishPrice;
            _absoluteDishPrice = absoluteDishPrice;

            _dishIngredientsAll = dishIngridientsAll;
            _dishSubdishes = dishSubdishes;
            _dishIngredients = dishIngredients;
        }
        public string GetDishName() { return _dishName; }
        public List<string> GetDishIngridientsAll() { return _dishIngredientsAll; }
        public int GetDishPrice() { return _dishPrice; }
        public void SetDishSubdishes(List<string> dishSubdishes) { _dishSubdishes = dishSubdishes; }
        public List<string> GetDishSubdish() { return _dishSubdishes; }
        public void SetDishIngredients (List<string> dishIngredients) { _dishIngredients = dishIngredients; }
        public List<string> GetDishIngredients () { return _dishIngredients; }
        public int GetAbsoluteDishPrice() { return _absoluteDishPrice; }
    }
}
