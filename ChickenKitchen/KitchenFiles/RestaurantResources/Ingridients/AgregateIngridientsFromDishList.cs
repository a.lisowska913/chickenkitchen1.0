﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ChickenKitchen
{
    public class AgregateIngridientsFromDishList
    {
        int iteratorIngridient = 0;

        List<string> basicIngridientInDishList = new List<string> ();   //wyodrębnione podstawowe składniki
        List<string> ingridientList = new List<string> ();              //niesprawdzone składniki

        List<string> subdishList = new List<string>();                  //wyodrębnione subdania

        //agreguje listę składników z danej potrawy
        public List<string> AgregateIngridientsFromList(string dishName, string[] foodAndIngridients, BasicIngridientsList basicIngridientList)
        {
            //dodaj danię na listę nie sprawdzonych składników
            ingridientList.Add(dishName);

            //wyodrębnianie podstawowych składników
            while (true)
            {
                //dla każdego elementu na liście składników dania, wyodrębnij podstawe składnki
                for (int j = 0; j < ingridientList.Count; j++)
                {
                    ingridientList[j] = ingridientList[j].Trim();
                    AgregateIngridientFromCurrentDish(foodAndIngridients, ingridientList[j], basicIngridientList);
                }

                //usuń sprawdzony składnik złożony z listy
                if (iteratorIngridient != 0) ingridientList.RemoveAt(iteratorIngridient);

                //jesli skończyły się niesprawdzone składniki, przerwij
                if (ingridientList.Count == 0) break;
            }
            return basicIngridientInDishList;
        }

        private void AgregateIngridientFromCurrentDish(string[] foodAndIngridients, string dishToCheck, BasicIngridientsList basicIngridientList)
        {
            foreach (string food in foodAndIngridients)
            {
                Regex regex = new Regex(@"(\D*): (\D*)");
                string currentDishName = regex.Match(food).Groups[1].Value;
                var currIngrident = regex.Match(food).Groups[2].Value;
                string[] currIngridientList = currIngrident.Split(',');

                for(int i=0;i<currIngridientList.Length; i++)
                {
                    currIngridientList[i] = currIngridientList[i].Trim();
                }

                //podział składników sprawdzanego dania na dania i podstawowe składniki
                if (currentDishName == dishToCheck) //iterowanie przez listę dań do nazwy sprawdzanego dania
                {
                    for (int i = 0; i < currIngridientList.Length; i++)
                    {
                        if (basicIngridientList.IsBasicIngridient(currIngridientList[i]))
                            basicIngridientInDishList.Add(currIngridientList[i]);
                        else
                            ingridientList.Add(currIngridientList[i]);

                        //usunięcie sprawdzonego dania z listy dań niesprawdzonych
                        for (int j = 0; j < ingridientList.Count; j++)
                        {
                            if (ingridientList[j] == dishToCheck)
                            {
                                ingridientList.RemoveAt(j);
                                break;
                            }
                        }
                        dishToCheck = "";
                    }
                }
            }
        }

        public List<string> AgregateSubdishesFromList(string dishName, string[] foodAndIngridients, DishList dishList)
        {

            List<string> subdishInDishList = new List<string>();
            foreach (string food in foodAndIngridients)
            {
                Regex regex = new Regex(@"(\D*): (\D*)");
                string currentDishName = regex.Match(food).Groups[1].Value;
                var currItem = regex.Match(food).Groups[2].Value;
                string[] currItemList = currItem.Split(',');

                for (int i = 0; i < currItemList.Length; i++)
                    currItemList[i] = currItemList[i].Trim();
                                                                                    //podział składników sprawdzanego dania na dania i podstawowe składniki
                if (currentDishName == dishName)                                    //iterowanie przez listę dań do nazwy sprawdzanego dania
                {
                    for (int i = 0; i < currItemList.Length; i++)
                    {
                        if (dishList.IsDish(currItemList[i]))
                            subdishInDishList.Add(currItemList[i]);
                    }
                }
            }
            return subdishInDishList;
        }

        public List<string> AgregateLeftIngredientsInThisLayer(string dishName, string[] foodAndIngridients,
            DishList dishList, BasicIngridientsList basicIngridientsList)
        {

            List<string> ingridientsInLayerList = new List<string>();
            foreach (string food in foodAndIngridients)
            {
                Regex regex = new Regex(@"(\D*): (\D*)");
                string currentDishName = regex.Match(food).Groups[1].Value;
                var currItem = regex.Match(food).Groups[2].Value;
                string[] currItemList = currItem.Split(',');

                for (int i = 0; i < currItemList.Length; i++)
                    currItemList[i] = currItemList[i].Trim();
                                                                                    //podział składników sprawdzanego dania na dania i podstawowe składniki
                if (currentDishName == dishName)                                    //iterowanie przez listę dań do nazwy sprawdzanego dania
                {
                    for (int i = 0; i < currItemList.Length; i++)
                    {
                        if (basicIngridientsList.IsBasicIngridient(currItemList[i]))
                            ingridientsInLayerList.Add(currItemList[i]);
                    }
                }
            }
            return ingridientsInLayerList;
        }
    }
}