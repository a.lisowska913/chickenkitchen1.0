﻿
namespace ChickenKitchen
{
    public class Ingridient
    {
        string _name;
        int _price;

        public Ingridient()
        {
            _name = "";
            _price = 0;
        }
        public Ingridient(string name, int price)
        {
            _name = name;
            _price = price;
        }
        public string GetIngridientName() { return _name; }
        public int GetIngridientPrice() { return _price; }
    }
}
