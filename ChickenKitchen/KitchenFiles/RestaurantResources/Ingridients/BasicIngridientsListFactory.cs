﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class BasicIngridientsListFactory
    {
        public BasicIngridientsList CreateBasicIngridientList(string[] basicIngridientFile)
        {
            List<Ingridient> basicIngridientList = new List<Ingridient>();

            foreach (string line in basicIngridientFile)
            {
                Regex regex = new Regex(@"(\D*): (\d+)");
                string ingridientName = regex.Match(line).Groups[1].Value;
                int ingridientPrice = Int32.Parse(regex.Match(line).Groups[2].Value);
                basicIngridientList.Add(new Ingridient(ingridientName, ingridientPrice));
            }
            return new BasicIngridientsList(basicIngridientList);
        }
    }
}
