﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;


namespace ChickenKitchen
{
    public class BasicIngridientsList
    {
        List<Ingridient> _basicIngridientList;

        public BasicIngridientsList(List<Ingridient> basicIngridientList)
        {
            _basicIngridientList = basicIngridientList;
        }

        public List<Ingridient> GetBasicIngridientList() { return _basicIngridientList; }
        public string[] GetBasticIngridientsNameList()
        {
            string[] basicIngridientList = new string[16];
            for (int i = 0; i<_basicIngridientList.Count(); i++)
            {
                basicIngridientList[i] = _basicIngridientList[i].GetIngridientName();
            }
            return basicIngridientList;
        }

        public int GetIngridientPriceFromList(string ingridientName)
        {
            for (int i = 0; i < _basicIngridientList.Count(); i++)
            {
                if (_basicIngridientList[i].GetIngridientName() == ingridientName) return _basicIngridientList[i].GetIngridientPrice(); 
            }
            return 0;
        }

        //sprawdź, czy element jest podstawowym składnikiem
        public bool IsBasicIngridient(string dishName)
        {
            for (int i = 0; i < _basicIngridientList.Count; i++)
            {
                if (dishName == _basicIngridientList[i].GetIngridientName()) return true;
            }
            return false;
        }

        //porównywanie obiektów
        public bool Equal(object? obj)
        {
            if (obj is not BasicIngridientsList) return false;

            BasicIngridientsList other = obj as BasicIngridientsList;
            if (this._basicIngridientList != other._basicIngridientList) return false;
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(this._basicIngridientList);
        }
    }
}


