﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class Config
    {
        public string order { get; set; }
        public string buy { get; set; }
        public string audit { get; set; }
        public string table { get; set; }
        public string budget { get; set; }
        public string morningstar { get; set; }
        public string throw_trash_away { get; set; }
        public int profit_margin { get; set; }
        public int transaction_tax { get; set; }
        public int daily_tax { get; set; } 
        public int every_third_discount { get; set; }
        public int total_maximum { get; set; }
        public int max_ingridient_type { get; set; }
        public int max_dish_type { get; set; }
        public string dishes_with_allergies { get; set; }
        public double spoil_rate { get; set; }
        public int waste_limit { get; set; }
        public int max_tip { get; set; }
        public int order_ingredient_volatility { get; set; }
        public int order_dish_volatility { get; set; }
        public int tip_tax { get; set; }
        public int waste_tax { get; set; }
        public Config()
        {
            order = "no";
            buy = "no";
            audit = "no";
            table = "no";
            budget = "no";
            morningstar = "no";
            throw_trash_away = "no";

            profit_margin = 30;
            transaction_tax = 10;
            daily_tax = 20;

            every_third_discount = 0;

            total_maximum = 500;
            max_ingridient_type = 10;
            max_dish_type = 3;

            dishes_with_allergies = "waste";

            spoil_rate = 0.1;

            waste_limit = 50;

            max_tip = 10;

            order_ingredient_volatility = 10;
            order_dish_volatility = 25;

            tip_tax = 5;
            waste_tax = 15;
        }

        public Config(string order, string buy, string audit, string table, string budget, string morningstar, string throw_trash_away,
            int profit_margin, int transaction_tax, int daily_tax, int every_third_discount, int total_maximum,
            int max_ingridient_type, int max_dish_type, string dishes_with_allergies, double spoil_rate, int waste_limit,
            int max_tip, int order_ingredient_volatility, int order_dish_volatility, int tip_tax, int waste_tax)
        {
            this.order = order;
            this.buy = buy;
            this.audit = audit;
            this.table = table;
            this.budget = budget;
            this.morningstar = morningstar;
            this.throw_trash_away = throw_trash_away;

            this.profit_margin = profit_margin;
            this.transaction_tax = transaction_tax;
            this.daily_tax = daily_tax;

            this.every_third_discount = every_third_discount;

            this.total_maximum = total_maximum;
            this.max_ingridient_type = max_ingridient_type;
            this.max_dish_type = max_dish_type;

            this.dishes_with_allergies = dishes_with_allergies;

            this.spoil_rate = spoil_rate;

            this.waste_limit = waste_limit;

            this.max_tip = max_tip;

            this.order_ingredient_volatility = order_ingredient_volatility;
            this.order_dish_volatility = order_dish_volatility;

            this.tip_tax = tip_tax;
            this.waste_tax = waste_tax;
        }
    }
}
