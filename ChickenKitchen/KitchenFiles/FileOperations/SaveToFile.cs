﻿using System;
using System.IO;

namespace ChickenKitchen
{
    public class SaveToFile
    {
        public string MakeRelativePath(string path)
        {
            Uri fullPath = new Uri(path);
            Uri relRoot = new Uri(@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\", UriKind.Absolute);

            string relPath = relRoot.MakeRelativeUri(fullPath).ToString();
            return relPath;
        }
        public void Save(string path, string[] fileInside)
        {
            StreamWriter outputFile = new StreamWriter(path);
            {
                foreach (string line in fileInside)
                {
                    outputFile.WriteLine(line);
                }
            }
            outputFile.Close();
        }
        public void SaveToAudit(string fileInside, string timeStamp)
        {
            string path = MakeRelativePath(@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\TxtFiles\Audit_" + timeStamp + ".txt" );
            StreamWriter outputFile = new (path, append: true);
            outputFile.WriteLineAsync(fileInside);
            outputFile.Close();
        }
        public void SaveToOutputFile(string fileInside)
        {
            string path = MakeRelativePath(@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\TxtFiles\outputFile.txt");
            StreamWriter outputFile = new(path, append: true);
            outputFile.WriteLineAsync(fileInside);
            outputFile.Close();
        }
        public void ClearOutputFile()
        {
            string path = MakeRelativePath(@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\TxtFiles\outputFile.txt");
            StreamWriter outputFile = new(path);
            outputFile.WriteLine("");
            outputFile.Close();
        }
        public void SaveToWasteFile(string fileInside)
        {
            string path = MakeRelativePath(@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\TxtFiles\wastePool.txt");
            StreamWriter outputFile = new(path);
            outputFile.WriteLine(fileInside);
            outputFile.Close();
        }
    }
}


/*
 * public void SaveToCustomerAndAllergies(string[] fileInside) //Customers And Allergies File
        {
            string path = MakeRelativePath(@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\TxtFiles\CostumersAndAllergies.txt");
            Save(path, fileInside);
        }

        public void SaveToBudget(string fileInside)                 //Budget File
        {
            string path = MakeRelativePath(@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\TxtFiles\RestaurantBudget.txt");
            StreamWriter outputFile = new StreamWriter(path);
            outputFile.WriteLine(fileInside);
            outputFile.Close();
        }*/
