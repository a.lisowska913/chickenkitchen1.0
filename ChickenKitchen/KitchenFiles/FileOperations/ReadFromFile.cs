﻿using System;
using System.IO;

namespace ChickenKitchen
{
    public class ReadFromFile
    {
        public string MakeRelativePath(string path)                                                                         //Make relative path
        {
            Uri fullPath = new Uri(path);
            Uri relRoot = new Uri
                (@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\", UriKind.Absolute);

            string relPath = relRoot.MakeRelativeUri(fullPath).ToString();
            return relPath;
        }
        public string[] ReadAllergiesFromFile()                                                                             //Customers And Allergies File
        {
            return System.IO.File.ReadAllLines
                (MakeRelativePath
                (@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\TxtFiles\CostumersAndAllergies.txt"));
        }
        public string[] ReadBasicIngredientsFromFile()                                                                      //Basic Ingridients File
        {
            return System.IO.File.ReadAllLines
                (MakeRelativePath
                (@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\TxtFiles\BaseIngredients.txt"));
        }
        public string[] ReadBudget()                                                                                        //Restaurant Budget File
        {
            return System.IO.File.ReadAllLines
                (MakeRelativePath
                (@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\TxtFiles\RestaurantBudget.txt"));
        }
        public string[] ReadFoodAndIngredientsFromFile()                                                                    //Food And Ingridients File
        {
            return System.IO.File.ReadAllLines
                (MakeRelativePath(@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\TxtFiles\FoodAndIngredients.txt"));
        }
        public string[] ReadInputOrderFromFile()                                                                            //input Order File
        {
            return System.IO.File.ReadAllLines
                (MakeRelativePath
                (@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\TxtFiles\inputOrder.txt"));
        }
        public string[] ReadWarehouseFromFile()                                                                             //Warehouse File
        {
            string path = "C:\\Users\\Admin\\source\\repos\\ChickenKitchen\\ChickenKitchen\\TxtFiles\\Warehouse.txt";
            if (File.Exists(path)) return System.IO.File.ReadAllLines(MakeRelativePath(@path));
            else
            {
                string[] result = new string[] { "File does not exist" };
                return result;
            }
        }

        public string ReadFromJsonCommandFile()                                                                                 //input Order File
        {
            string path = MakeRelativePath(@"C:\Users\Admin\source\repos\ChickenKitchen\ChickenKitchen\KitchenFiles\Config\Config.json");
            string jsonString = File.ReadAllText(path);
            return jsonString;
        }
    }
}
