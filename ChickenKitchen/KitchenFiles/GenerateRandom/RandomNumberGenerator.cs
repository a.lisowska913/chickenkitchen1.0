﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen
{

    public interface GiveRandomNumber
    {
        int RandomNumber(Config config);
    }
    public class RandomNumberGenerator_ForSpoiling : GiveRandomNumber
    {
        public int RandomNumber(Config config)
        {
            Random random = new Random();
            int generatedNumber = random.Next(1, 1001);
            return generatedNumber;
        }
    }
    public class RandomNumberGenerator_ForTipPercentage : GiveRandomNumber
    {
        public int RandomNumber(Config config)
        {
            Random random = new Random();
            int generatedNumber = random.Next(1, config.max_tip + 1);
            return generatedNumber;
        }
    }
    public class RandomNumberGenerator_ForTipChance : GiveRandomNumber
    {
        public int RandomNumber(Config config)
        {
            Random random = new Random();
            int generatedNumber = random.Next(1, 101);
            return generatedNumber;
        }
    }

    public class RandomNumberGenerator_ForIngridientVolatility : GiveRandomNumber
    {
        public int RandomNumber(Config config)
        {
            Random random = new Random();
            int generatedNumber = random.Next(0, config.order_ingredient_volatility);
            random = new Random();
            int mark = random.Next(1, 3);
            if (mark == 1) return generatedNumber;
            if (mark == 2) return -generatedNumber;
            return 0;
        }
    }

    public class RandomNumberGenerator_ForDishVolatility : GiveRandomNumber
    {
        public int RandomNumber(Config config)
        {
            Random random = new Random();
            int generatedNumber = random.Next(0, config.order_dish_volatility);
            random = new Random();
            int mark = random.Next(1, 3);
            if (mark == 1) return generatedNumber;
            if (mark == 2) return -generatedNumber;
            return 0;
        }
    }

    public class FakeRandom_NumberGenerator1 : GiveRandomNumber
    {
        public int RandomNumber(Config config)
        {
            int generatedNumber;
            return generatedNumber = 1;
        }
    }
    public class FakeRandom_NumberGenerator5 : GiveRandomNumber
    {
        public int RandomNumber(Config config)
        {
            int generatedNumber;
            return generatedNumber = 5;
        }
    }
    public class FakeRandom_NumberGenerator10 : GiveRandomNumber
    {
        public int RandomNumber(Config config)
        {
            int generatedNumber;
            return generatedNumber = 10;
        }
    }
    public class FakeRandom_NumberGenerator100 : GiveRandomNumber
    {
        public int RandomNumber(Config config)
        {
            int generatedNumber;
            return generatedNumber = 100;
        }
    }

}
