﻿using System.Collections.Generic;

namespace ChickenKitchen
{
    public class Client
    {
        List<string> _order;

        string _clientName;
        string[] _clientAllergies;

        int _clientFunds;
        int _numberOfOrders;
        int _tipAmount;

        bool _isOrderDone;
        bool _isOrderFail;

        public Client()
        {
            _clientName = "";
            _clientAllergies = new string[] { "none" };
            _clientFunds = 0;
            _isOrderDone = false;
            _isOrderFail = false;
            _order = new List<string>();
            _numberOfOrders = 0;
        }
        public Client(string clientName, string[] clientAllergies, int clientFunds)
        {
            _clientName = clientName;
            _clientAllergies = clientAllergies;
            _clientFunds = clientFunds;
            _isOrderDone = false;
            _isOrderFail = false;
            _order = new List<string>();
            _numberOfOrders = 0;
        }

        public string GetClientName() { return _clientName; }
        public string[] GetClientAllergies() { return _clientAllergies; }
        public int GetClientFunds() { return _clientFunds; }
        public void SetClientFunds(int value){ _clientFunds = value; }
        public bool GetIsOrderDone() { return _isOrderDone; }
        public void SetIsOrderDone(bool isOrderDone) { _isOrderDone = isOrderDone; }
        public bool GetIsOrderFail() { return _isOrderFail; }
        public void SetIsOrderFail(bool isOrderFail) { _isOrderFail = isOrderFail; }
        public void SetOrder(List<string> orderList) { _order = orderList; }
        public List<string> GetOrder() { return _order; }
        public int IncrementNumberOfOrder() { return ++_numberOfOrders; }
        public void SetNumberOfOrder(int value) { _numberOfOrders = value; }
        public void SetTipAmount(int value) { _tipAmount = value; }
        public int GetTipAmount() { return _tipAmount; }

    }
}