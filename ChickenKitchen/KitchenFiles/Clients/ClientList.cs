﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ChickenKitchen
{
    public class ClientList
    {
        public List<Client> _clientsList;

        public ClientList(List<Client> clientList)
        {
            _clientsList = clientList;
        }
        public List<Client> GetClientList() { return _clientsList; }
        public void SetClientList(Client client) { _clientsList.Add(client); }
        public int GetClienListLength() { int length = _clientsList.Count(); return length; }
        public Client GetClient(string clientName)                                                          //zwraca klienta po imieniu
        {
            foreach (Client client in _clientsList)
            {
                if (clientName == client.GetClientName())
                {
                    return client;
                }
            }
            Client clientRet = new Client();
            return clientRet;
        }
        public void SetClientFunds(string clientName, int value)                                         //aktualizacja funduszy klienta
        {
            foreach (Client client in _clientsList)
            {
                if (clientName == client.GetClientName())
                {
                    client.SetClientFunds(value);
                }
            }
        }
        public int GetClientFunds(string clientName)                                                      //sprawdzanie funduszy klienta
        {
            foreach (Client client in _clientsList)
            {
                if (clientName == client.GetClientName())
                {
                    return client.GetClientFunds();
                }
            }
            return -1;
        }
        public string GetClientIsOrderFail(string clientName)                                             //czy klient nie zawalił zamówienia
        {
            foreach (Client client in _clientsList)
            {
                if (clientName == client.GetClientName())
                {
                    if (client.GetIsOrderFail()) return "fail";
                    else return "success";
                }
            }
            return "client not found";
        } 

        public bool GetIsThirdOrder_AfterOrder(string clientName)
        {
            foreach (Client client in _clientsList)
            {
                if (clientName == client.GetClientName())
                {
                    if (client.IncrementNumberOfOrder() == 3) return true;
                    else return false;
                }
            }
            return false;
        }

        public void SetNumberOfOrder(string clientName, int value)
        {
            foreach (Client client in _clientsList)
            {
                if (clientName == client.GetClientName())
                {
                    client.SetNumberOfOrder(value);
                }
            }
        }

        //porównywanie obiektów
        public bool Equal(object? obj)
        {
            if (obj is not ClientList) return false;

            ClientList other = obj as ClientList;
            if (this._clientsList != other._clientsList) return false;
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(this._clientsList);
        }
    }
}
