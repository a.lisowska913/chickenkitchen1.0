﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ChickenKitchen
{
    public class ClientListFactory
    {
        public ClientList CreateClientList(string[] customersAndAllergiesAndFundsFile)
        {
            string currentCustomerName;
            string[] currentCustomerAllergies;
            int currentCustomerFunds;
            List<Client> clientList = new List<Client>();

            foreach (string line in customersAndAllergiesAndFundsFile)
            {
                Regex regex = new Regex(@"(\D*): (\D*) : (\d+)");
                currentCustomerName = regex.Match(line).Groups[1].Value;
                if (currentCustomerName == "") break;
                currentCustomerAllergies = new AgregateAllergyListFromCustomer().Agregate(currentCustomerName, customersAndAllergiesAndFundsFile);
                currentCustomerFunds = CheckFundsByCustomer(currentCustomerName, customersAndAllergiesAndFundsFile);
                Client currentClient = new Client(currentCustomerName, currentCustomerAllergies, currentCustomerFunds);
                clientList.Add(currentClient);
            }
            return new ClientList(clientList);
        }
        private int CheckFundsByCustomer(string customerName, string[] customersAndAllergies)       //sprawdzanie funduszy,
        {                                                                                           //poprzez sprawdzenie początkowych wartości w pliku

            int funds = 0;
            foreach (string line in customersAndAllergies)
            {
                Regex regex = new Regex(@"(\D*): (\D*) : (\d+)");
                string currName = regex.Match(line).Groups[1].Value;
                if (currName == customerName)
                {
                    funds = Int32.Parse(regex.Match(line).Groups[3].Value);
                }
            }
            return funds;
        }
    }
}
