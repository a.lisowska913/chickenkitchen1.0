﻿using System;
using System.Text.RegularExpressions;

namespace ChickenKitchen
{
    public class FundsOperations
    {
        public int PayForYourMeal                                                                   //Sprawdzenie ile klientowi zostanie pieniędzy
            (string customerName, ClientList clientList, BasicIngridientsList basicIngridientList,
            int dishPrice)
        {
            int customerFunds = clientList.GetClientFunds(customerName) - dishPrice;
            clientList.SetClientFunds(customerName, customerFunds);
            return customerFunds;
        }

        public bool IsFundsEnoughToBuyDish                                                          //czy klient ma dość funduszy do zakupu dania
            (string clientName, string dishName, DishList dishList, ClientList clientList,
            BasicIngridientsList basicIngridientList, string[] customersAndAllergies)
        {
            int customerFunds = clientList.GetClientFunds(clientName);
            int dishPrice = dishList.GetDishPrice(dishName);
            if (customerFunds > dishPrice) return true;
            else return false;
        }
    }
}

