﻿using System.Text.RegularExpressions;


namespace ChickenKitchen
{
    public class AgregateAllergyListFromCustomer
    {
        //zagreguj listę alergii danej osoby
        public string[] Agregate(string costumerName, string[] customersAndAllergies)
        {
            string[] allergyList = new string[4];

            string currentCostumerName = "";
            string allergicIngrident = "";

            foreach (string line in customersAndAllergies)
            {
                Regex regex = new Regex(@"(\D*): (\D*) : (\d+)");
                currentCostumerName = regex.Match(line).Groups[1].Value;
                if (currentCostumerName == costumerName)
                {
                    allergicIngrident = regex.Match(line).Groups[2].Value;
                    allergyList = allergicIngrident.Split(',');
                }
            }
            return allergyList;
        }
    }
}


