﻿using System.Collections.Generic;

namespace ChickenKitchen
{
    public class CheckAllergiesForIngredient
    {
        //sprawdza, czy osoba ma alergię na jeden ze składników
        public string CheckIfAllergic(string costumerName, string dishName, ClientList clientList, DishList dishList, BasicIngridientsList basicIngridientsList)
        {
            Client currentClient = clientList.GetClient(costumerName);
            //agregacja danych z plików tekstowych
            string[] allergyIngridients = currentClient.GetClientAllergies();
            List<string> dishIngridients = dishList.GetDishIngridientsAll(dishName);

            //sprawdzenie czy ma alergię na jeden ze składników
            string allergy = "none";

            for (int i = 0; i < allergyIngridients.Length; i++)
            {
                for (int j = 0; j < dishIngridients.Count; j++)
                {
                    if (allergyIngridients[i] != null || dishIngridients[j] != null)
                    {
                        if (allergyIngridients[i] == dishIngridients[j])
                        {
                            allergy = allergyIngridients[i];
                            break;
                        }
                    }
                }
            }
            return allergy;
        }
    }
}
