﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace ChickenKitchen
{
    class ChickenKitchen
    {
        static void Main(string[] args)
        {
            //Zczytywanie z plików
            string[] basicIngredientsFile = new ReadFromFile().ReadBasicIngredientsFromFile();      //podstawowe składniki z pliku
            string[] customersAndAllergiesAndFundsFile = new ReadFromFile().ReadAllergiesFromFile();//alergie, klienci i fundusze z pliku
            string[] foodAndIgredientsFile = new ReadFromFile().ReadFoodAndIngredientsFromFile();   //dania i składniki z pliku
            string[] inputOrderFromFile = new ReadFromFile().ReadInputOrderFromFile();              //zamówienie z pliku
            string[] restaurantBudgetFile = new ReadFromFile().ReadBudget();                        //budżet z pliku
            string[] warehouseFile = new ReadFromFile().ReadWarehouseFromFile();                    //zawartość magazynu z pliku
            string jsonString = new ReadFromFile().ReadFromJsonCommandFile();                       //zawartość pliku configuracyjnego

            string compactedWarehouseFile = string.Join("", warehouseFile);                         //konwersja pliku magazynu do jednej linii

            //Inicjalizacja
            Config config = new Config();
            config = JsonSerializer.Deserialize<Config>(jsonString);                                //deserializacja danych z pliku json
            int currentBudget =                                                                     //Ustawianie budżetu
                new RestaurantBudget().GetBudgetValueFromFile(restaurantBudgetFile);
            RestaurantBudget restaurantBudget = new RestaurantBudget(currentBudget);                //Ustawianie budżetu
            ClientList clientList =                                                                 //Ustawiania danych klientów
                new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);        
            BasicIngridientsList basicIngridientsList =                                             //Ustawianie listy podstawowych składników
                new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList =                                                                     //Ustawianie listy dań, ich składników i cen
                new DishListFactory().CreateDishList(foodAndIgredientsFile,
                basicIngridientsList, config);
            Warehouse warehouse =                                                                   //Ustawienia początkowe magazynu
                new WarehouseFactory().
                CreateInitialWarehouse(basicIngridientsList, dishList, compactedWarehouseFile);
            PerformAuditCommand audit = new PerformAuditCommand();

            int random = new RandomNumberGenerator_ForSpoiling().RandomNumber(config);

            //Reset outputu
            new SaveToFile().ClearOutputFile();

            //Wyświetlanie na konsoli outputu
            bool isAuditOn = false;
            int iteratorOrderNumber = 1;                                                            //iterator dla numeru zamówienia
            string timeStamp = "";                                                                  //timestamp do tworzenia kolejnych plików audytu

            //Wpis początkowy
            Console.WriteLine("Restaurant budget: " + restaurantBudget.GetRestaurantBudget());      //początkowy budżet restauracji
            new SaveToFile().SaveToOutputFile("Restaurant budget: " + 
                restaurantBudget.GetRestaurantBudget());

            //Wpisy odnośnie zamówień
            foreach (string whoBuyWhat in inputOrderFromFile)
            {   
                currentBudget = restaurantBudget.GetRestaurantBudget();                             //pobiera obecny budżet restauracji
                int budgetBeforeOperations = currentBudget;

                string[] whoBuyWhatTable = whoBuyWhat.Split(",");
                string command = whoBuyWhatTable[0];
                string mark = "";
                if (whoBuyWhatTable.Length > 1) mark = whoBuyWhatTable[1];

                int randomChanceToGiveTip = new RandomNumberGenerator_ForTipChance().RandomNumber(config);
                int randomTipPercentage = new RandomNumberGenerator_ForTipPercentage().RandomNumber(config);
                int randomIngredientVolatility = new RandomNumberGenerator_ForIngridientVolatility().RandomNumber(config);
                int randomDishVolatility = new RandomNumberGenerator_ForDishVolatility().RandomNumber(config);

                if (warehouse.GetThrashAmount() > config.waste_limit)
                {
                    Console.WriteLine("RESTAURANT POISONED");
                }
                else if (currentBudget <= 0)                                                        //czy restauracja ma jeszcze budżet
                {
                    if(command == "Budget" && (mark == "+" || mark == "="))
                    {
                        string result = new OperationFromCommand().CheckRecipe(whoBuyWhat,
                        customersAndAllergiesAndFundsFile, dishList,
                        currentBudget, restaurantBudget, clientList,
                        basicIngridientsList, warehouse,
                        config, random, randomChanceToGiveTip, randomTipPercentage,
                        randomIngredientVolatility, randomDishVolatility, isAuditOn,
                        audit) +
                        System.Environment.NewLine;

                        result = "Order " + iteratorOrderNumber + ": " + result;
                        Console.WriteLine(result);                                                  //wyświetl rezultat zamówienia
                        new SaveToFile().SaveToOutputFile(result);
                        iteratorOrderNumber++;                                                      //zwiększenie numeru zamówienia o 1
                    }
                    else Console.WriteLine("RESTAURANT BANKRUPT");                                  //jeśli nie, ogłoś bankructwo
                }
                else                                                                                //jeśli tak, przyjmij zamówienie
                {
                    string result = new OperationFromCommand().CheckRecipe(whoBuyWhat,
                        customersAndAllergiesAndFundsFile, dishList,
                        currentBudget, restaurantBudget, clientList,
                        basicIngridientsList, warehouse,
                        config, random, randomChanceToGiveTip, randomTipPercentage,
                        randomIngredientVolatility, randomDishVolatility, isAuditOn,
                        audit) +
                        System.Environment.NewLine;                                                 //sprawdzam, czy osoba może dostać dane danie

                    if (result == "Audit is on\r\n")                                                //reakcja na komendę audytu
                    {
                        isAuditOn = true;
                        timeStamp = DateTime.Now.ToFileTime().ToString();
                        string initAuditRecord = audit.MakeAuditInitRecord(warehouse, restaurantBudget, config);
                        new SaveToFile().SaveToAudit(initAuditRecord, timeStamp);
                    }
                    else
                    {
                        result = "Order " + iteratorOrderNumber + ": " + result;
                        Console.WriteLine(result);                                                  //wyświetl rezultat zamówienia
                        new SaveToFile().SaveToOutputFile(result);
                        iteratorOrderNumber++;                                                      //zwiększenie numeru zamówienia o 1
                    }
                    if (command == "Buy" || command == "Table") clientList.GetClient(mark).SetTipAmount(0);
                }
            }

            //Wpis końcowy
            if (warehouse.GetThrashAmount() > config.waste_limit) Console.WriteLine("RESTAURANT POISONED");
            int dailyTax = new BudgetOperations().DailyTax(config, restaurantBudget);
            int tips = restaurantBudget.GetCollectedTips();
            int tipsTax = (tips * config.tip_tax)/100;
            int wasteTax = calculateWasteTax(basicIngridientsList, dishList, config, warehouse);
            restaurantBudget.SetRestaurantBudget(restaurantBudget.GetRestaurantBudget() + tips - wasteTax);
            if (isAuditOn)
            {
                for (int i = 0; i < audit.GetFromMemory().Count; i++)
                {
                    new SaveToFile().SaveToAudit(audit.GetFromMemory()[i], timeStamp);
                }
                new SaveToFile().SaveToAudit("Tips: " + tips, timeStamp);
                new SaveToFile().SaveToAudit("Tips tax: " + tipsTax, timeStamp);
                new SaveToFile().SaveToAudit("Waste tax: " + wasteTax, timeStamp);
                new SaveToFile().SaveToAudit("Daily tax: " + dailyTax, timeStamp);
                new SaveToFile().SaveToAudit("AUDIT END", timeStamp);
            }
            Console.WriteLine("Daily tax: " + dailyTax);
            Console.WriteLine("Tips: " + tips);
            Console.WriteLine("Tips tax: " + tipsTax);
            Console.WriteLine("Waste tax: " + wasteTax);
            Console.WriteLine("Restaurant budget: " + restaurantBudget.GetRestaurantBudget());      //końcowy budżet restauracji
            new SaveToFile().SaveToOutputFile("Restaurant budget: " + 
                restaurantBudget.GetRestaurantBudget());
            new SaveToFile().SaveToOutputFile("Daily tax: " + dailyTax);
        }

        private static int calculateWasteTax(BasicIngridientsList basicIngridientsList, DishList dishList, Config config, Warehouse warehouse)
        {
            int wasteTax = 0;
            int totalWasteCost = warehouse.GetWasteTotalCost(basicIngridientsList, dishList, config);
            int environmentFine = ((totalWasteCost - (totalWasteCost % 100))/100)*20;
            wasteTax = (totalWasteCost * config.waste_tax) / 100 + environmentFine;
            return wasteTax;
        }
    }
}