﻿using System;
using System.Text.RegularExpressions;

namespace ChickenKitchen
{
    public class OperationFromCommand
    {
        public string CheckRecipe(string whoBuyWhat, string[] customersAndAllergiesAndFundsFile, DishList dishList,
            int restaurantBudget, RestaurantBudget restaurantBudgetClass, ClientList clientList, BasicIngridientsList basicIngridientsList,
            Warehouse warehouse, Config config, int random, int randomChanceToGiveTip, int randomTipPercentage,
            int randomIngredientVolatility, int randomDishVolatility, bool isAuditOn, PerformAuditCommand audit)
        {
            //odczytanie komendy
            string[] whoBuyWhatSplitted = whoBuyWhat.Split(",");
            string command = whoBuyWhatSplitted[0];

            switch (command)
            {
                case "Buy":                                                                                                 //zakup dania przez klienta
                    if (config.buy == "no") return "Buy command disabled";
                    return new PerformBuyCommand().PerformOperation_CustomerBuyingDish(whoBuyWhat,
                        customersAndAllergiesAndFundsFile, dishList, restaurantBudget, restaurantBudgetClass,
                        clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip,
                        randomTipPercentage, isAuditOn, audit);
                case "Budget":                                                                                              //zmiany budżetu
                    if (config.budget == "no") return "Budget command disabled";
                    return new PerformBudgetCommand().ChangeBudget(whoBuyWhat, restaurantBudget, restaurantBudgetClass,
                        isAuditOn, audit, warehouse, config);
                case "Order":                                                                                               //zamówienia do magazynu
                    if (config.order == "no") return "Order command disabled";
                    return new PerformOrderCommand().OrderToWarehouse(whoBuyWhat, restaurantBudget, restaurantBudgetClass,
                        basicIngridientsList, dishList, warehouse, config, random, randomIngredientVolatility,
                        randomDishVolatility, audit, isAuditOn);
                case "Table":                                                                                               //zamówienia od stolików
                    if (config.table == "no") return "Table command disabled";
                    return new PerformTableCommand().PerformOperations_MultipleCustomerBuyingDish(whoBuyWhat,
                        customersAndAllergiesAndFundsFile, dishList, restaurantBudget, restaurantBudgetClass,
                        clientList, basicIngridientsList, warehouse, config,
                        random, randomChanceToGiveTip, randomTipPercentage, isAuditOn, audit);
                case "Audit":                                                                                               //wykonanie audytu
                    if (config.audit == "no") return "Audit command disabled";
                    return audit.InitAudit();
                case "Morningstar":
                    if (config.morningstar == "no") return "Morningstar command disabled";
                    else return "Customer is dead.";
                case "Throw trash away":                                                                                   //wyrzucenie śmieci
                    if (config.throw_trash_away == "no") return "Throw trash away disabled";
                    else  return new PerformThrowCommand().Throw(warehouse, isAuditOn, audit, restaurantBudgetClass, config);
                default:                                                                                                    //jeśli komenda jest niepoprawna
                    return "Incorrect command or structure!";
            }
        }
    }
}