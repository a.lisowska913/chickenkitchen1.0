﻿using NUnit.Framework;
using ChickenKitchen;
using SemanticComparison;
using System.Collections.Generic;

namespace TestKitchen
{
    public class TestClients
    {
        [Test]
        public void A_TestClientName()
        {
            //Given
            string name = "Elon Carousel";
            string[] allergies = { "Vinegar", "Olives" };
            int funds = 100;
            Client client = new Client(name, allergies, funds);
            string expected = "Elon Carousel";

            //When
            string actual = client.GetClientName();

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void B_TestClientAllergies()
        {
            //Given
            string name = "Elon Carousel";
            string[] allergies = { "Vinegar", "Olives" };
            int funds = 100;
            Client client = new Client(name, allergies, funds);
            string[] expected = { "Vinegar", "Olives" };

            //When
            string[] actual = client.GetClientAllergies();

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void C_TestClientFunds()
        {
            //Given
            string name = "Elon Carousel";
            string[] allergies = { "Vinegar", "Olives" };
            int funds = 100;
            Client client = new Client(name, allergies, funds);
            int expected = 100;

            //When
            int actual = client.GetClientFunds();

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void D_TestCreateClientList()
        {
            //Given
            string[] customersAndAllergiesAndFundsFile = { "Elon Carousel: Vinegar,Olives : 100", "Julie Mirage: Soy : 50" };

            List<Client> clientList = new List<Client>();
            clientList.Add(new Client("Elon Carousel", new string[] { "Vinegar", "Olives" }, 100));
            clientList.Add(new Client("Julie Mirage", new string[] { "Soy" }, 50));
            ClientList expected = new ClientList(clientList);

            //When
            ClientList actual = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            //Likeness<ClientList, ClientList> actualClientList = new Likeness<ClientList, ClientList>(actual);

            //Then
            actual.Equal(expected);
        }
    }
}
