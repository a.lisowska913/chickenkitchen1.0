﻿using NUnit.Framework;
using System.Collections.Generic;
using ChickenKitchen;
using System;

namespace TestKitchen
{
    public class TestPerformBuyAllergiesAndWarehouse
    {
        [Test]
        public void A_TestAllergicPerson_ConfigSetDishWaste_ShouldGiveWaste()
        {
            //Given
            string order = "Buy, Bernard Unfortunate, Emperor Chicken";
            string expected = "Bernard Unfortunate - Emperor Chicken: can't order, allergic to: Potatoes\r\nDish wasted!";

            string[] basicIngredientsFile = { "Feta: 7", "Vinegar: 1", "Rice: 2", "Chocolate: 5", "Chicken: 20", "Tuna: 25", "Potatoes: 3", "Asparagus: 50", "Milk: 5", "Honey: 15", "Paprika: 4", "Garlic: 3", "Water: 1", "Lemon: 2", "Tomatoes: 4", "Pickles: 2" };
            string[] customersAndAllergiesAndFundsFile = { "Bernard Unfortunate: Potatoes : 15", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Emperor Chicken: Fat Cat Chicken,Spicy Sauce,Tuna Cake", "Tuna Cake: Tuna,Chocolate,Youth Sauce",
                "Fries: Potatoes", "Diamond Salad: Tomatoes,Pickles,Feta", "Youth Sauce: Asparagus,Milk,Honey",
                "Princess Chicken: Chicken,Youth Sauce", "Fat Cat Chicken: Princess Chicken,Youth Sauce,Fries,Diamond Salad",
                "Spicy Sauce: Paprika,Garlic,Water" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random,
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void B_TestAllergicPerson_ConfigSetDishKeep_ShouldGiveKeep()
        {
            //Given
            string order = "Buy, Bernard Unfortunate, Emperor Chicken";
            string expected = "Bernard Unfortunate - Emperor Chicken: can't order, allergic to: Potatoes\r\nDish keeped!";

            string[] basicIngredientsFile = { "Feta: 7", "Vinegar: 1", "Rice: 2", "Chocolate: 5", "Chicken: 20", "Tuna: 25", "Potatoes: 3", "Asparagus: 50", "Milk: 5", "Honey: 15", "Paprika: 4", "Garlic: 3", "Water: 1", "Lemon: 2", "Tomatoes: 4", "Pickles: 2" };
            string[] customersAndAllergiesAndFundsFile = { "Bernard Unfortunate: Potatoes : 15", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Emperor Chicken: Fat Cat Chicken,Spicy Sauce,Tuna Cake", "Tuna Cake: Tuna,Chocolate,Youth Sauce",
                "Fries: Potatoes", "Diamond Salad: Tomatoes,Pickles,Feta", "Youth Sauce: Asparagus,Milk,Honey",
                "Princess Chicken: Chicken,Youth Sauce", "Fat Cat Chicken: Princess Chicken,Youth Sauce,Fries,Diamond Salad",
                "Spicy Sauce: Paprika,Garlic,Water" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "keep", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random,
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void C_TestAllergicPerson_ConfigSetDishKeep_CantAffordIt()
        {
            //Given
            string order = "Buy, Bernard Unfortunate, Fries";
            string expected = "Bernard Unfortunate - Fries: can't order, allergic to: Potatoes\r\nDish wasted!";

            string[] basicIngredientsFile = { "Feta: 7", "Vinegar: 1", "Rice: 2", "Chocolate: 5", "Chicken: 20", "Tuna: 25", "Potatoes: 3", "Asparagus: 50", "Milk: 5", "Honey: 15", "Paprika: 4", "Garlic: 3", "Water: 1", "Lemon: 2", "Tomatoes: 4", "Pickles: 2" };
            string[] customersAndAllergiesAndFundsFile = { "Bernard Unfortunate: Potatoes : 15", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Emperor Chicken: Fat Cat Chicken,Spicy Sauce,Tuna Cake", "Tuna Cake: Tuna,Chocolate,Youth Sauce",
                "Fries: Potatoes", "Diamond Salad: Tomatoes,Pickles,Feta", "Youth Sauce: Asparagus,Milk,Honey",
                "Princess Chicken: Chicken,Youth Sauce", "Fat Cat Chicken: Princess Chicken,Youth Sauce,Fries,Diamond Salad",
                "Spicy Sauce: Paprika,Garlic,Water" };
            string warehouseFile = "Potatoes, 10";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 1, "keep", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random,
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void D_TestAllergicPerson_ConfigSetDishNumber_ShouldGiveKeep()
        {
            //Given
            string order = "Buy, Bernard Unfortunate, Emperor Chicken";
            string expected = "Bernard Unfortunate - Emperor Chicken: can't order, allergic to: Potatoes\r\nDish keeped!";

            string[] basicIngredientsFile = { "Feta: 7", "Vinegar: 1", "Rice: 2", "Chocolate: 5", "Chicken: 20", "Tuna: 25", "Potatoes: 3", "Asparagus: 50", "Milk: 5", "Honey: 15", "Paprika: 4", "Garlic: 3", "Water: 1", "Lemon: 2", "Tomatoes: 4", "Pickles: 2" };
            string[] customersAndAllergiesAndFundsFile = { "Bernard Unfortunate: Potatoes : 15", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Emperor Chicken: Fat Cat Chicken,Spicy Sauce,Tuna Cake", "Tuna Cake: Tuna,Chocolate,Youth Sauce",
                "Fries: Potatoes", "Diamond Salad: Tomatoes,Pickles,Feta", "Youth Sauce: Asparagus,Milk,Honey",
                "Princess Chicken: Chicken,Youth Sauce", "Fat Cat Chicken: Princess Chicken,Youth Sauce,Fries,Diamond Salad",
                "Spicy Sauce: Paprika,Garlic,Water" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "20", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random,
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void E_TestAllergicPerson_ConfigSetDishNumber_ShouldGiveWaste()
        {
            //Given
            string order = "Buy, Bernard Unfortunate, Emperor Chicken";
            string expected = "Bernard Unfortunate - Emperor Chicken: can't order, allergic to: Potatoes\r\nDish wasted!";

            string[] basicIngredientsFile = { "Feta: 7", "Vinegar: 1", "Rice: 2", "Chocolate: 5", "Chicken: 20", "Tuna: 25", "Potatoes: 3", "Asparagus: 50", "Milk: 5", "Honey: 15", "Paprika: 4", "Garlic: 3", "Water: 1", "Lemon: 2", "Tomatoes: 4", "Pickles: 2" };
            string[] customersAndAllergiesAndFundsFile = { "Bernard Unfortunate: Potatoes : 15", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Emperor Chicken: Fat Cat Chicken,Spicy Sauce,Tuna Cake", "Tuna Cake: Tuna,Chocolate,Youth Sauce",
                "Fries: Potatoes", "Diamond Salad: Tomatoes,Pickles,Feta", "Youth Sauce: Asparagus,Milk,Honey",
                "Princess Chicken: Chicken,Youth Sauce", "Fat Cat Chicken: Princess Chicken,Youth Sauce,Fries,Diamond Salad",
                "Spicy Sauce: Paprika,Garlic,Water" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "200000", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random,
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}
