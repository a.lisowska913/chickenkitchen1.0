﻿using NUnit.Framework;
using ChickenKitchen;
using System;

namespace TestKitchen
{
    class TestPerformTableBuy
    {
        [Test]
        public void A_TestTable_OnePerson_OneDish()
        {
            //Given
            string order = "Table, Julie Mirage,Princess Chicken";
            string expected = "\r\nJulie Mirage - Princess Chicken: success You have to pay: 9 - can't order, budget 8\r\nTable failed. Money Returned";

            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1", "Tomatoes: 4" };
            string[] customersAndAllergiesAndFundsFile = { "Julie Mirage: Soy : 8" };
            string[] foodAndIgredientsFile = { "Princess Chicken: Lemon,Water,Tomatoes" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random, 
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void B_TestTable_TwoPerson_TwoDishes_ShouldGive_OrderCorrect()
        {
            //Given
            string order = "Table, Alexandra Smith,Adam Smith,Irish Fish,Fries";
            string expected = "\r\nAlexandra Smith - Irish Fish: success You have to pay: 9, bought correctly! Tip payed: 0. Money left: 1 Tax: 0\r\n" +
                "Sufficient amount in warehouse.\r\n" +
                "Adam Smith - Fries: success You have to pay: 3, bought correctly! Tip payed: 0. Money left: 97 Tax: 0\r\n" +
                "Sufficient amount in warehouse.";

            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1", "Tomatoes: 4", "Potatoes: 2", };
            string[] customersAndAllergiesAndFundsFile = { "Alexandra Smith: none : 10", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Irish Fish: Lemon,Water,Tomatoes", "Fries: Potatoes"};
            string warehouseFile = "Irish Fish, 100, Fries, 100";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random, 
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void C_TestTable_TwoPerson_TwoDishes_OneAllergic_OrderFail_ShouldGive_Allergic()
        {
            //Given
            string order = "Table, Alexandra Smith,Bernard Unfortunate,Irish Fish,Fries";
            string expected = "\r\nAlexandra Smith - Irish Fish: success You have to pay: 9, bought correctly! Tip payed: 0. Money left: 1 Tax: 0\r\n" +
                "Sufficient amount in warehouse.\r\n" +
                "Bernard Unfortunate - Fries: can't order, allergic to: Potatoes" +
                "\r\nDish wasted!" +
                "\r\nTable failed. Money Returned";

            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1", "Tomatoes: 4", "Potatoes: 2", };
            string[] customersAndAllergiesAndFundsFile = { "Alexandra Smith: none : 10", "Bernard Unfortunate: Potatoes : 100" };
            string[] foodAndIgredientsFile = { "Irish Fish: Lemon,Water,Tomatoes", "Fries: Potatoes" };
            string warehouseFile = "Irish Fish, 100, Fries, 100";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random, 
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void D_TestTable_TwoPerson_OneDish_OrderFail_ShouldGive_IncorrectDish()
        {
            //Given
            string order = "Table, Alexandra Smith,Adam Smith,Irish Fish";
            string expected = "\r\nAlexandra Smith - Irish Fish: success You have to pay: 9, bought correctly! Tip payed: 0. Money left: 1 Tax: 0\r\n" +
                "Sufficient amount in warehouse.\r\n" +
                "Incorrect dish!" +
                "\r\nTable failed. Money Returned";

            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1", "Tomatoes: 4", "Potatoes: 2", };
            string[] customersAndAllergiesAndFundsFile = { "Alexandra Smith: none : 10", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Irish Fish: Lemon,Water,Tomatoes", "Fries: Potatoes" };
            string warehouseFile = "Irish Fish, 100";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random, 
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void E_TestTable_OnePerson_TwoDish_OrderFail_ShouldGive_IncorrectCustomer()
        {
            //Given
            string order = "Table, Alexandra Smith,Irish Fish,Fries";
            string expected = "\r\nAlexandra Smith - Irish Fish: success You have to pay: 9, bought correctly! Tip payed: 0. Money left: 1 Tax: 0\r\n" +
                "Sufficient amount in warehouse.\r\n" +
                "Incorrect customer!" +
                "\r\nTable failed. Money Returned";

            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1", "Tomatoes: 4", "Potatoes: 2", };
            string[] customersAndAllergiesAndFundsFile = { "Alexandra Smith: none : 10", "Bernard Unfortunate: Potatoes : 100" };
            string[] foodAndIgredientsFile = { "Irish Fish: Lemon,Water,Tomatoes", "Fries: Potatoes" };
            string warehouseFile = "Irish Fish, 100, Fries, 100";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random, 
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void F_TestTable_OnePersonOrderTwice_OrderFail_ShouldGive_SameCustomerFail()
        {
            //Given
            string order = "Table, Adam Smith,Adam Smith,Fries,Fries";
            string expected = "\r\nAdam Smith - Fries: success You have to pay: 3, bought correctly! Tip payed: 0. Money left: 17 Tax: 0\r\n" +
                "Sufficient amount in warehouse." +
                "\r\nAdam Smith - Fries: success You have to pay: 3, bought correctly! Tip payed: 0. Money left: 14 Tax: 0" +
                "\r\nSufficient amount in warehouse." +
                "\r\nTable failed. Money Returned";

            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1", "Tomatoes: 4", "Potatoes: 2", };
            string[] customersAndAllergiesAndFundsFile = { "Adam Smith: none : 20", "Bernard Unfortunate: Potatoes : 100" };
            string[] foodAndIgredientsFile = { "Irish Fish: Lemon,Water,Tomatoes", "Fries: Potatoes" };
            string warehouseFile = "Fries, 100";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random, 
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void F_TestTable_ThreeNextOrdersFormSamePerson_ShouldBeDiscountOnThird()
        {
            //Given
            string order = "Table, Aleksandra Smith, Irish Fish";
            string expected = "\r\nAleksandra Smith - Irish Fish: success You have to pay: 14, but you earn 10 discount!\r\n" +
                "You will pay: 13, bought correctly! Tip payed: 0. Money left: 1987 Tax: 1\r\n" +
                "Sufficient amount in warehouse.";

            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1", "Tomatoes: 4", "Potatoes: 2", "Tuna: 2"};
            string[] customersAndAllergiesAndFundsFile = { "Aleksandra Smith: none : 2000" };
            string[] foodAndIgredientsFile = { "Irish Fish: Lemon, Water, Tomatoes, Potatoes, Tuna" };
            string warehouseFile = "Irish Fish, 100";
            string customerName = "Aleksandra Smith";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 10, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            clientList.SetNumberOfOrder(customerName, 2);

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random,
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}
