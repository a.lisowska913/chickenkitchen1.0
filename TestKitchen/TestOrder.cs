﻿using NUnit.Framework;
using ChickenKitchen;
using System;

namespace TestKitchen
{
    class TestOrder
    {

        [Test]
        public void A_TestOrderIngredients_EnoughSpace()
        {
            //Given
            string order = "Order, Tuna, 300";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("all", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 5000, 1000, 3000, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Bought successfuly! It costs: 3300\r\n\r\nOrder end.";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config,
                random, randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void B_TestOrderIngredients_RestaurantBudgetChangeCorrectly()
        {
            //Given
            string order = "Order, Tuna, 300";
            string[] basicIngrdientsFile = { "Tuna: 20" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 5000, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget(restaurantBudgetFile);
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            int expected = -5600;

            //When
            new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random,
                randomVolatility, randomVolatility, isAuditOn, audit);

            int actual = restaurantBudget.GetRestaurantBudget();

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void C_TestOrderIngredients_NotEnoughSpace()
        {
            //Given
            string order = "Order, Tuna, 300";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 5000, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Wasted: 295 Tuna (limit: 10)\r\nCosts of transaction: 3300\r\n\r\nOrder end.";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void D_TestOrderIngredients_NotEnoughSpace_ConfigIngridients_ShouldGiveNotEnoughSpace()
        {
            //Given
            string order = "Order, Tuna, 300";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 5000, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Wasted: 295 Tuna (limit: 10)\r\nCosts of transaction: 3300\r\n\r\nOrder end.";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void E_TestOrderDishes_NotEnoughSpace_ConfigDishes_ShouldGiveNotEnoughSpace()
        {
            //Given
            string order = "Order, Tuna Cake, 300";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { "Tuna Cake: Tuna" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 5000, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Wasted: 297 Tuna Cake (limit: 3)\r\nCosts of transaction: 3300\r\n\r\nOrder end.";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void F_TestOrderIngredients_EnoughSpace_ConfigIngredients_ShouldGiveCorrect()
        {
            //Given
            string order = "Order, Tuna, 3";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 5000, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Bought successfuly! It costs: 33\r\n\r\nOrder end.";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void G_TestOrderIngredients_EnoughSpace_ConfigDishes_ShouldGiveIncorrectOrder()
        {
            //Given
            string order = "Order, Tuna, 300";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("dish", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 5000, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Incorrect dish!";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void H_TestOrderDishes_EnoughSpace_ConfigDishes_ShouldGiveCorrect()
        {
            //Given
            string order = "Order, Tuna Cake, 1";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { "Tuna Cake: Tuna, Tuna" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 5000, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Bought successfuly! It costs: 22\r\n\r\nOrder end.";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void I_TestOrderDishes_EnoughSpace_ConfigIngredients_ShouldGiceIncorrectOrder()
        {
            //Given
            string order = "Order, Tuna Cake, 1";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { "Tuna Cake: Tuna, Tuna" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("ingredient", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 5000, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Incorrect ingridient!";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void J_TestOrderIngredients_EnoughSpace_ConfigAll_ShouldGiveCorect()
        {
            //Given
            string order = "Order, Tuna, 1";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 5000, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Bought successfuly! It costs: 11\r\n\r\nOrder end.";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);
            //Then
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void K_TestOrderIngredients_NotEnoughSpaceInTotal()
        {
            //Given
            string order = "Order, Tuna, 100";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "Tuna, 0";

            int restaurantBudgetFile = 1000;

            Config config = new Config("all", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 10, 100, 300, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            string expected = "Wasted: 90 Tuna (warehouse limit: 10)\r\n\r\nCosts of transaction: 1100\r\n\r\nOrder end.";

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void L_TestOrderIngredients_NotEnoughSpaceBoth_InTotal_AndForIngridients()
        {
            //Given
            string order = "Order, Tuna, 100";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "Tuna, 0";

            int restaurantBudgetFile = 1000;

            Config config = new Config("all", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 10, 5, 5, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            string expected = "Wasted: 90 Tuna (warehouse limit: 10)\r\nWasted: 95 Tuna (limit: 5)\r\nCosts of transaction: 1100\r\n\r\nOrder end.";

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void M_TestOrderMultipleIngredients()
        {
            //Given
            string order = "Order, Tuna, 20, Tuna Cake, 10, Potatoes, 3";
            string[] basicIngrdientsFile = { "Tuna: 10", "Potatoes: 5" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { "Tuna Cake: Tuna, Potatoes" };
            string warehouseFile = "Tuna, 0";


            int restaurantBudgetFile = 1000;

            Config config = new Config("all", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 1000, 50, 50, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Bought successfuly! It costs: 220\r\nBought successfuly! It costs: 165\r\nBought successfuly! It costs: 16\r\n\r\nOrder end.";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void N_TestOrderIngredient_WithIngredientVolatility10()
        {
            //Given
            string order = "Order, Tuna, 20";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "Tuna, 0";

            int restaurantBudgetFile = 1000;

            Config config = new Config("all", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 1000, 50, 50, "waste", 0, 500, 0, 10, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomIngredientVolatility = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Bought successfuly! It costs: 222\r\n\r\nOrder end.";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomIngredientVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void O_TestOrderDish_WithDishVolatility25()
        {
            //Given
            string order = "Order, Tuna Cake, 20";
            string[] basicIngrdientsFile = { "Tuna: 10", "Potatoes: 5" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { "Tuna Cake: Tuna, Potatoes" };
            string warehouseFile = "Tuna, 0";

            int restaurantBudgetFile = 1000;

            Config config = new Config("all", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 1000, 50, 50, "waste", 0, 500, 0, 0, 25, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomDishVolatility = 25;
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Bought successfuly! It costs: 412\r\n\r\nOrder end.";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomDishVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}
