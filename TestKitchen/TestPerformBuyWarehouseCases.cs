﻿using ChickenKitchen;
using NUnit.Framework;
using System;

namespace TestKitchen
{
    class TestPerformBuyWarehouseCases
    {
        [Test]
        public void A_TestWarehouse_SufficientAmountInWarehouse_ShouldGive_SufficientAmount()
        {
            //Given
            string order = "Buy, Adam Smith, Fries";
            string expected = "Adam Smith - Fries: success You have to pay: 4, bought correctly!"+
                " Tip payed: 0. Money left: 96" +
                " Tax: 0"+
                "\r\nSufficient amount in warehouse.";

            string[] basicIngredientsFile = { "Potatoes: 3" };
            string[] customersAndAllergiesAndFundsFile = { "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Fries: Potatoes" };
            string warehouseFile = "Fries, 100";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random,
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void B_TestWarehouse_InufficientDish_SufficienIngridients_ShouldGive_SufficientAmount()
        {
            //Given
            string order = "Buy, Adam Smith, Fries";
            string expected = "Adam Smith - Fries: success You have to pay: 4, bought correctly!" +
                " Tip payed: 0. Money left: 96" +
                " Tax: 0" +
                "\r\nSufficient amount in warehouse.";

            string[] basicIngredientsFile = { "Potatoes: 3" };
            string[] customersAndAllergiesAndFundsFile = { "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Fries: Potatoes" };
            string warehouseFile = "Potatoes, 100";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random,
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void C__TestWarehouse_InsufficientDish_InSufficientIngridients_ShouldGive_InsufficientAmount()
        {
            //Given
            string order = "Buy, Adam Smith, Fries";
            string expected = "Adam Smith - Fries: success You have to pay: 4, sufficient funds.\r\n" +
                "Amount in warehouse insufficient. Can't order.";

            string[] basicIngredientsFile = { "Potatoes: 3" };
            string[] customersAndAllergiesAndFundsFile = { "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Fries: Potatoes" };
            string warehouseFile = "Fries, 0";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random,
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void D_TestWarehouse_InufficientDish_SufficienIngridients_MultiDependencies_ShouldGive_SufficientAmount()
        {
            //Given
            string order = "Buy, Adam Smith, Tuna Cake";
            string expected = "Adam Smith - Tuna Cake: success You have to pay: 14, bought correctly!" +
                " Tip payed: 0. Money left: 6" +
                " Tax: 1" +
                "\r\nSufficient amount in warehouse.";

            string[] basicIngredientsFile = { "Chockolate: 1", "Tuna: 2", "Oil: 3", "Flour: 4", "Water: 1" };
            string[] customersAndAllergiesAndFundsFile = { "Adam Smith: none : 20" };
            string[] foodAndIgredientsFile = { "Tuna Cake: Chockolate, Tuna Can, Cake", "Tuna Can: Tuna, Oil", "Cake: Flour, Water" };
            string warehouseFile = "Chockolate, 10, Tuna Cake, 3, Flour, 2, Water, 2";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random,
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}
