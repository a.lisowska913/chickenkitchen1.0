﻿using NUnit.Framework;
using ChickenKitchen;
using System;

namespace TestKitchen
{
    public class TestRestaurantBudget
    {
        [Test]
        public void A_TestRestaurantBudget_Change()
        {
            //Given
            string order = "Budget, =, 300";
            string expected = "Budget changed to: 300";

            string[] basicIngredientsFile = { };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomVolatility = 0;

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random, randomVolatility,
                randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void B_TestRestaurantBudget_Add()
        {
            //Given
            string order = "Budget, +, 300";
            string expected = "Budget changed! Added: 300";

            string[] basicIngredientsFile = { };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random, 
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void C_TestRestaurantBudget_Lose()
        {
            //Given
            string order = "Budget, -, 300";
            string expected = "Budget changed! Loses: 300";

            string[] basicIngredientsFile = { };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 1000;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, random, 
                randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}
