﻿using NUnit.Framework;
using System.Collections.Generic;
using ChickenKitchen;

namespace TestKitchen
{
    public class TestWarehouse
    {
        [Test]
        public void A_TestSetInitialWarehouseWithFile()
        {
            //Given
            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1" };
            string[] foodAndIgredientsFile = { "Omega Sauce: Lemon,Water" };
            string warehouseFile = "Tuna, 10";

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);

            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);

            List<WarehouseItem> expected = new List<WarehouseItem>();
            expected.Add(new WarehouseItem("Lemon", 0, "ingridient"));
            expected.Add(new WarehouseItem("Water", 0, "ingridient"));
            expected.Add(new WarehouseItem("Omega Sauce", 0, "dish"));

            //When
            Warehouse actual = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            //Then
            Equals(expected, actual);
        }

        [Test]
        public void B_TestMakeWarehouseFormFileWithNoFile()
        {
            //Given
            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1"};
            string[] foodAndIgredientsFile = { "Omega Sauce: Lemon,Water"};
            string warehouseFile = "File does not exist";

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);

            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);

            List<WarehouseItem> expected = new List<WarehouseItem>();
            expected.Add(new WarehouseItem("Lemon", 5, "ingridient"));
            expected.Add(new WarehouseItem("Water", 5, "ingridient"));
            expected.Add(new WarehouseItem("Omega Sauce", 0, "dish"));

            //When
            Warehouse actual = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);


            //Then
            Equals(expected, actual);
        }

        [Test]
        public void C_TestMakeWarehouseFormFile_WithFile()
        {
            //Given
            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1" };
            string[] foodAndIgredientsFile = { "Omega Sauce: Lemon,Water" };
            string warehouseFile = "Water, 10";

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);

            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);

            List<WarehouseItem> expected = new List<WarehouseItem>();
            expected.Add(new WarehouseItem("Lemon", 0, "ingridient"));
            expected.Add(new WarehouseItem("Water", 10, "ingridient"));
            expected.Add(new WarehouseItem("Omega Sauce", 0, "dish"));

            //When
            Warehouse actual = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            //Then
            Equals(expected, actual);
        }

        [Test]
        public void D_TestMakeWarehouseFormFile_WithNoFile()
        {
            //Given
            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1" };
            string[] foodAndIgredientsFile = { "Omega Sauce: Lemon,Water" };
            string warehouseFile = "File does not exist";


            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);

            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);

            List<WarehouseItem> expected = new List<WarehouseItem>();
            expected.Add(new WarehouseItem("Lemon", 5, "ingridient"));
            expected.Add(new WarehouseItem("Water", 5, "ingridient"));
            expected.Add(new WarehouseItem("Omega Sauce", 0, "dish"));

            //When
            Warehouse actual = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);


            //Then
            Equals(expected, actual);
        }
    }
}
