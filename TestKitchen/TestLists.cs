﻿using NUnit.Framework;
using ChickenKitchen;
using System.Collections.Generic;

namespace TestKitchen
{
    public class TestLists
    {
        [Test]
        public void A_TestAgregateAllergyList()
        {
            //Given
            string order = "Elon Carousel";
            string[] expected = { "Vinegar", "Olives" };

            string[] customersAndAllergies = { "Julie Mirage: Soy : 100", "Elon Carousel: Vinegar,Olives : 50", "Adam Smith: none : 100" };

            //When
            string[] actual = new AgregateAllergyListFromCustomer().Agregate(order, customersAndAllergies);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void B_TestBasicIngridientList()
        {
            //Given
            string[] expected = new string[16];
            expected[0] = "Chicken";
            expected[1] = "Tuna";
            expected[2] = "Potatoes";
            expected[3] = "Asparagus";

            string[] basicIngridientFile = { "Chicken: 20", "Tuna: 25", "Potatoes: 3", "Asparagus: 50" };

            //When
            BasicIngridientsList actual = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngridientFile);

            //Then
            Equals(expected, actual);
        }

        [Test]
        public void C_TestCheckIfBasicIngridient()
        {
            //Given
            string order = "Potatoes";
            bool expected = true;
            string[] basicIngridientsFile = { "Tuna: 1", "Potatoes: 2", "Tomatoes: 3 " };
            BasicIngridientsList basicIngridientList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngridientsFile);

            //When
            bool actual = basicIngridientList.IsBasicIngridient(order);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void D_TestIngridientsList_SimpeDish()
        {
            //Given
            string order = "Irish Fish";
            List<string> expected = new List<string>();
            expected.Add("Tuna");
            expected.Add("Potatoes");
            expected.Add("Potatoes");

            string[] basicIngredientsFile = { "Tuna: 1", "Potatoes: 2", "Tomatoes: 3", "Vinegar: 4", "Chockolate: 5" };
            string[] foodAndIngredients = { "Irish Fish: Tuna,Fries,Smashed Potatoes", "Fries: Potatoes",
                "Ruby Salad: Tomatoes,Vinegar,Chocolate", "Smashed Potatoes: Potatoes" };

            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);

            //When
            List<string> actual = new AgregateIngridientsFromDishList().AgregateIngridientsFromList(order, foodAndIngredients, basicIngridientsList);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void E_TestIngridientsList_ComplexDish()
        {
            //Given
            string order = "Emperor Chicken";
            List<string> expected = new List<string>();
            expected.Add("Paprika");
            expected.Add("Garlic");
            expected.Add("Chicken");
            expected.Add("Water");
            expected.Add("Tuna");

            string[] basicIngredientsFile = { "Chicken: 1", "Tuna: 2", "Potatoes: 3", "Asparagus: 4", "Paprika: 5", "Garlic: 6", "Water: 7" };
            string[] foodAndIngredients = { "Emperor Chicken: Nugget,Paprika",
                "Nugget: Lol Sauce,Garlic,Chicken", "Lol Sauce: Tuna Can,Water", "Tuna Can: Tuna" };

            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);

            //When
            List<string> actual = new AgregateIngridientsFromDishList().AgregateIngridientsFromList(order, foodAndIngredients, basicIngridientsList);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void F_TestDishList()
        {
            //Given
            string[] foodAndIngridientsFile = { "Emperor Chicken: Nugget,Paprika", "Nugget: Chicken", };
            string[] basicIngredientsFile = { "Chicken: 1", "Tuna: 2", "Potatoes: 3", "Asparagus: 4", "Paprika: 5", "Garlic: 6", "Water: 7" };

            List<Dish> expected = new List<Dish>()
            {
                new Dish("Emperor Chicken", new List<string>(){ "Chicken", "Paprika" }, new List<string>(){"Nugget" }, 8, 6, new List<string>{"Paprika" }),
                new Dish("Nugget", new List<string>(){"Chicken" }, new List<string>(), 1, 1, new List<string>{"Chicken" })
            };

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 5000, 0, 0, 0, 0, 0);

            BasicIngridientsList basicIngredientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);

            //When
            DishList actual = new DishListFactory().CreateDishList(foodAndIngridientsFile, basicIngredientsList, config);

            //Then
            Equals(expected, actual);

        }
    }
}
