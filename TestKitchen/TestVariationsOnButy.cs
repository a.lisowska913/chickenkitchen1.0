﻿using ChickenKitchen;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestKitchen
{
    public class TestVariationsOnButy
    {
        [Test]
        public void A_TestCorrectOrder_CustomerWantToGiveTip_EnoughMoney_Percentage10()
        {
            //Given
            string order = "Buy, Aleksandra Smith, Tuna Cake";
            string expected = "Aleksandra Smith - Tuna Cake: success You have to pay: 46, bought correctly!" +
                " Tip payed: 4." +
                " Money left: 50 Tax: 4\r\nSufficient amount in warehouse.";

            string[] basicIngredientsFile = { "Tuna: 25", "Chockolate: 10" };
            string[] customersAndAllergiesAndFundsFile = { "Aleksandra Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Tuna Cake: Tuna, Chockolate" };
            string warehouseFile = "Tuna Cake, 3";
            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 10, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomTipPercentage = new FakeRandom_NumberGenerator10().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, randomChanceToGiveTip, 
                randomTipPercentage, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void A_TestCorrectOrder_CustomerWantToGiveTip_EnoughMoney_Percentage5()
        {
            //Given
            string order = "Buy, Aleksandra Smith, Tuna Cake";
            string expected = "Aleksandra Smith - Tuna Cake: success You have to pay: 46, bought correctly!" +
                " Tip payed: 2." +
                " Money left: 52 Tax: 4\r\nSufficient amount in warehouse.";

            string[] basicIngredientsFile = { "Tuna: 25", "Chockolate: 10" };
            string[] customersAndAllergiesAndFundsFile = { "Aleksandra Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Tuna Cake: Tuna, Chockolate" };
            string warehouseFile = "Tuna Cake, 3";
            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 10, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomTipPercentage = new FakeRandom_NumberGenerator5().RandomNumber(config);

            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random, 
                randomChanceToGiveTip, randomTipPercentage, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}
