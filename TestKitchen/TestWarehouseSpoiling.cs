﻿using NUnit.Framework;
using ChickenKitchen;
using System;

namespace TestKitchen
{
    public class TestWarehouseSpoiling
    {
        [Test]
        public void A_TestWarehouseSpoiling()
        {
            //Given
            int expected = 100;

            string[] basicIngredientsFile = { "Potatoes: 3", "Tomatoes: 1" };
            string[] customersAndAllergiesAndFundsFile = { "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Fries: Potatoes" };
            string warehouseFile = "Potatoes, 100, Tomatoes, 100";

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 1, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            //When
            int actual = new WarehouseSpoiling().CheckIfIngredientsArentSpoiled(warehouse, config, random);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void B_TestWasteOverflow()
        {
            //Given
            string order = "Order, Tuna, 300";
            string[] basicIngrdientsFile = { "Tuna: 10" };
            string[] customersAndAllergiesAndFundsFile = { };
            string[] foodAndIgredientsFile = { };
            string warehouseFile = "Tuna, 10";

            int restaurantBudgetFile = 1000;

            Config config = new Config("all", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 0, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);

            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngrdientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            string expected = "Wasted: 300 Tuna (limit: 10)Thrash is full! You have poisoned the kitchen!\r\nCosts of transaction: 3300\r\n\r\nOrder end.";

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList,
                restaurantBudgetFile, restaurantBudget, clientList, basicIngridientsList, warehouse, config, 
                random, random, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}
