using NUnit.Framework;
using ChickenKitchen;
using System;

namespace TestKitchen
{
    public class TestPerformBuy
    {
        [Test]
        public void A_TestPersonAllergic_EnoughMoney_SimpleDish_ShouldGive_Allergic()
        {
            //Given
            string order = "Buy, Elon Carousel, Fish In Water";
            string expected = "Elon Carousel - Fish In Water: can't order, allergic to: Vinegar\r\nDish wasted!";

            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1", "Tomatoes: 4", "Vinegar: 1", "Chockolate: 5", "Tuna: 25", "Potatoes: 3" };
            string[] customersAndAllergiesAndFundsFile = { "Elon Carousel: Vinegar,Olives : 50" };
            string[] foodAndIgredientsFile = { "Omega Sauce: Lemon,Water", "Ruby Salad: Tomatoes,Vinegar,Chockolate", "Fish In Water: Tuna,Omega Sauce,Ruby Salad" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void B_TestPersonAllergic_NotEnoughMoney_ComplexDish_ShouldGive_Allergic()
        {
            //Given
            string order = "Buy, Bernard Unfortunate, Emperor Chicken";
            string expected = "Bernard Unfortunate - Emperor Chicken: can't order, allergic to: Potatoes\r\nDish wasted!";

            string[] basicIngredientsFile = { "Feta: 7", "Vinegar: 1", "Rice: 2", "Chocolate: 5", "Chicken: 20", "Tuna: 25", "Potatoes: 3", "Asparagus: 50", "Milk: 5", "Honey: 15", "Paprika: 4", "Garlic: 3", "Water: 1", "Lemon: 2", "Tomatoes: 4", "Pickles: 2" };
            string[] customersAndAllergiesAndFundsFile = { "Bernard Unfortunate: Potatoes : 15", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Emperor Chicken: Fat Cat Chicken,Spicy Sauce,Tuna Cake", "Tuna Cake: Tuna,Chocolate,Youth Sauce",
                "Fries: Potatoes", "Diamond Salad: Tomatoes,Pickles,Feta", "Youth Sauce: Asparagus,Milk,Honey",
                "Princess Chicken: Chicken,Youth Sauce", "Fat Cat Chicken: Princess Chicken,Youth Sauce,Fries,Diamond Salad",
                "Spicy Sauce: Paprika,Garlic,Water" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        
        [Test]
        public void C_TestPersonNotAllergic_EnoughMoney_SimpleDish_ShouldGive_EverythingCorrect()
        {
            //Given
            string order = "Buy, Adam Smith, Fries";
            string expected = "Adam Smith - Fries: success You have to pay: 4, bought correctly!"+
                " Tip payed: 0. Money left: 96 Tax: 0\r\nSufficient amount in warehouse.";

            string[] basicIngredientsFile = { "Tuna: 25", "Potatoes: 3", "Water: 1", "Lemon: 2", "Tomatoes: 4", "Pickles: 2" };
            string[] customersAndAllergiesAndFundsFile = { "Bernard Unfortunate: Potatoes : 15", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = {  "Fries: Potatoes" };
            string warehouseFile = "Fries, 100";
            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void D_TestPersonNotAllergic_EnoughMoney_ComplexDish_ShouldGive_EverythingCorrect()
        {
            //Given
            string order = "Buy, Julie Mirage, Fish In Water";
            string expected = "Julie Mirage - Fish In Water: success You have to pay: 49, bought correctly!"+
                " Tip payed: 0. Money left: 51 Tax: 4\r\nSufficient amount in warehouse.";

            string[] basicIngredientsFile = { "Vinegar: 1", "Chockolate: 5", "Chicken: 20", "Tuna: 25", "Water: 1", "Lemon: 2", "Tomatoes: 4"  };
            string[] customersAndAllergiesAndFundsFile = { "Julie Mirage: Soy : 100", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = {"Omega Sauce: Lemon,Water", "Ruby Salad: Tomatoes,Vinegar,Chockolate", "Fish In Water: Tuna,Omega Sauce,Ruby Salad" };
            string warehouseFile = "Fish In Water, 100";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

    
        [Test]
        public void E_TestPersonNotAllergic_NotEnoughMoney_ComplexDish_ShouldGive_NorEnoughBudget()
        {
            //Given
            string order = "Buy, Julie Mirage, Emperor Chicken";
            string expected = "Julie Mirage - Emperor Chicken: success You have to pay: 369 - can't order, budget 100";

            string[] basicIngredientsFile = { "Feta: 7", "Vinegar: 1", "Rice: 2", "Chocolate: 5", "Chicken: 20", "Tuna: 25", "Potatoes: 3", "Asparagus: 50", "Milk: 5", "Honey: 15", "Paprika: 4", "Garlic: 3", "Water: 1", "Lemon: 2", "Tomatoes: 4", "Pickles: 2" };
            string[] customersAndAllergiesAndFundsFile = { "Julie Mirage: Soy : 100", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Emperor Chicken: Fat Cat Chicken,Spicy Sauce,Tuna Cake", "Tuna Cake: Tuna,Chocolate,Youth Sauce",
                "Fries: Potatoes", "Diamond Salad: Tomatoes,Pickles,Feta", "Youth Sauce: Asparagus,Milk,Honey",
                "Princess Chicken: Chicken,Youth Sauce", "Fat Cat Chicken: Princess Chicken,Youth Sauce,Fries,Diamond Salad",
                "Spicy Sauce: Paprika,Garlic,Water" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void F_TestPersonNotAllergic_NotEnoughMoney_SimpleDish_ShouldGiveNotEnoughBudget()
        {
            //Given
            string order = "Buy, Bernard Unfortunate, Youth Sauce";
            string expected = "Bernard Unfortunate - Youth Sauce: success You have to pay: 91 - can't order, budget 15";

            string[] basicIngredientsFile = { "Potatoes: 3", "Asparagus: 50", "Milk: 5", "Honey: 15", "Paprika: 4", "Garlic: 3", "Water: 1", "Lemon: 2", "Tomatoes: 4", "Pickles: 2" };
            string[] customersAndAllergiesAndFundsFile = { "Bernard Unfortunate: Potatoes : 15", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Youth Sauce: Asparagus,Milk,Honey" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void G_TestIncorrectOrder_BadStructure()
        {
            //Given
            string order = "Sadkl,jaslkdjasldkjaskldjsa";
            string expected = "Incorrect command or structure!";

            string[] basicIngredientsFile = { "Feta: 7", "Vinegar: 1", "Rice: 2", "Chocolate: 5", "Olives: 10", "Chicken: 20", "Tuna: 25", "Potatoes: 3", "Asparagus: 50", "Milk: 5", "Honey: 15", "Paprika: 4", "Garlic: 3", "Water: 1", "Lemon: 2", "Tomatoes: 4", "Pickles: 2" };
            string[] customersAndAllergiesAndFundsFile = { "Bernard Unfortunate: Potatoes : 15", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Feta Salad: Feta" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
        
        [Test]
        public void H_TestIncorrectOrder_WrongDish()
        {
            //Given
            string order = "Buy, Adam Smith, nsdkfk";
            string expected = "Incorrect dish!";

            string[] basicIngredientsFile = { "Feta: 7", "Vinegar: 1", "Rice: 2", "Chocolate: 5", "Olives: 10" };
            string[] customersAndAllergiesAndFundsFile = { "Julie Mirage: Soy : 100", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = { "Feta Salad: Feta" };
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void I_TestIncorrectOrder_WrongCustomer()
        {
            //Given
            string order = "Buy, JulieMirage, ";
            string expected = "Incorrect customer!";

            string[] basicIngredientsFile = { "Feta: 7", "Vinegar: 1", "Rice: 2"};
            string[] customersAndAllergiesAndFundsFile = { "Bernard Unfortunate: Potatoes : 15", "Adam Smith: none : 100" };
            string[] foodAndIgredientsFile = {  "Feta Salad: Feta"};
            string warehouseFile = "File does not exist";

            int restaurantBudgetFile = 0;

            
            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void J_CheckIfAllergic_FindAllergenIngridient()
        {
            //Given
            string order = "Buy, Elon Carousel, Fish In Water";
            string expected = "Elon Carousel - Fish In Water: can't order, allergic to: Vinegar\r\nDish wasted!";

            string[] customersAndAllergiesAndFundsFile = { "Elon Carousel: Vinegar,Olives : 50" };
            string[] foodAndIgredientsFile = { "Omega Sauce: Lemon,Water", "Ruby Salad: Tomatoes,Vinegar,Chockolate", "Fish In Water: Tuna,Omega Sauce,Ruby Salad" };
            string[] basicIngredientsFile = { "Lemon: 2", "Water: 1", "Tomatoes: 4", "Vinegar: 1", "Chockolate: 5", "Tuna: 25", "Potatoes: 3" };
            string warehouseFile = "Fish In Water, 100";

            int restaurantBudgetFile = 0;

            Config config = new Config("yes", "yes", "yes", "yes", "yes", "no", "yes", 30, 10, 20, 0, 500, 10, 3, "waste", 0, 500, 0, 0, 0, 0, 0);
            int random = new FakeRandom_NumberGenerator1().RandomNumber(config);
            int randomChanceToGiveTip = new FakeRandom_NumberGenerator100().RandomNumber(config);
            int randomVolatility = 0;

            RestaurantBudget restaurantBudget = new RestaurantBudget();
            ClientList clientList = new ClientListFactory().CreateClientList(customersAndAllergiesAndFundsFile);
            BasicIngridientsList basicIngridientsList = new BasicIngridientsListFactory().CreateBasicIngridientList(basicIngredientsFile);
            DishList dishList = new DishListFactory().CreateDishList(foodAndIgredientsFile, basicIngridientsList, config);
            Warehouse warehouse = new WarehouseFactory().CreateInitialWarehouse(basicIngridientsList, dishList, warehouseFile);

            bool isAuditOn = false;
            PerformAuditCommand audit = new PerformAuditCommand();

            //When
            string actual = new OperationFromCommand().CheckRecipe(order, customersAndAllergiesAndFundsFile, dishList, restaurantBudgetFile,
                restaurantBudget, clientList, basicIngridientsList, warehouse, config, random,
                randomChanceToGiveTip, random, randomVolatility, randomVolatility, isAuditOn, audit);

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}

