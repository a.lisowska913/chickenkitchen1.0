﻿using NUnit.Framework;
using ChickenKitchen;

namespace TestKitchen
{
    public class TestFiles
    {

        [Test]
        public void TestReadAllergiesFile()
        {
            //Given
            string[] expected = new string[16];
            expected[0] = "Julie Mirage: Soy : 100";
            expected[1] = "Elon Carousel: Vinegar,Olives : 50";

            //When
            string[] actual = new string[16];
            actual = new ReadFromFile().ReadAllergiesFromFile();

            //Then
            Assert.AreEqual(expected[0], actual[0]);
            Assert.AreEqual(expected[1], actual[1]);
        }

        [Test]
        public void TestReadFoodAndIngredientsFromFile()
        {
            //Given
            string[] expected = new string[16];
            expected[0] = "Emperor Chicken: Fat Cat Chicken,Spicy Sauce,Tuna Cake";
            expected[1] = "Fat Cat Chicken: Princess Chicken,Youth Sauce,Fries,Diamond Salad";

            //When
            string[] actual = new string[16];
            actual = new ReadFromFile().ReadFoodAndIngredientsFromFile();

            //Then
            Assert.AreEqual(expected[0], actual[0]);
            Assert.AreEqual(expected[1], actual[1]);
        }

        [Test]
        public void TestReadBasicIngredientsFromFile()
        {
            //Given
            string[] expected = new string[16];
            expected[0] = "Chicken: 20";
            expected[1] = "Tuna: 25";


            //When
            string[] actual = new string[16];
            actual = new ReadFromFile().ReadBasicIngredientsFromFile();

            //Then
            Assert.AreEqual(expected[0], actual[0]);
            Assert.AreEqual(expected[1], actual[1]);
        }

    }
}
